<?php

namespace App\Providers;

// use Illuminate\Support\Facades\Gate;
use App\Models\Birthday;
use App\Models\Category;
use App\Models\Event;
use App\Models\Notification;
use App\Models\Routine;
use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('update-or-delete-birthday', fn (User $u, Birthday $b) => $u->id == $b->user->id);
        Gate::define('update-or-delete-event', fn (User $u, Event $e) => $u->id == $e->user->id);
        Gate::define('update-or-delete-routine', fn (User $u, Routine $r) => $u->id == $r->user->id);
        Gate::define('update-or-delete-task', fn (User $u, Task $t) => $u->id == $t->category->user->id);
        Gate::define('update-or-delete-task-category', fn (User $u, Category $c) => $u->id == $c->user->id);
        Gate::define('confirm-notification', fn (User $u, Notification $n) => $u->id == $n->user->id);
    }
}
