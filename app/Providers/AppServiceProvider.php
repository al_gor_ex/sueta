<?php

namespace App\Providers;

use App\Service\Birthday\BirthdayService;
use App\Service\Birthday\IBirthdayService;
use App\Service\Calendar\CalendarService;
use App\Service\Calendar\ICalendarService;
use App\Service\Event\EventService;
use App\Service\Event\IEventService;
use App\Service\Notification\INotificationService;
use App\Service\Notification\NotificationService;
use App\Service\Routine\IRoutineService;
use App\Service\Routine\RoutineService;
use App\Service\Task\CategoryService;
use App\Service\Task\ICategoryService;
use App\Service\Task\ITaskService;
use App\Service\Task\TaskService;
use App\Service\User\IUserService;
use App\Service\User\UserService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        IBirthdayService::class => BirthdayService::class,
        ICalendarService::class => CalendarService::class,
        ICategoryService::class => CategoryService::class,
        IEventService::class => EventService::class,
        INotificationService::class => NotificationService::class,
        IRoutineService::class => RoutineService::class,
        ITaskService::class => TaskService::class,
        IUserService::class => UserService::class
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
