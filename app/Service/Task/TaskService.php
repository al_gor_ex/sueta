<?php

namespace App\Service\Task;

use App\Http\Dto\Requests\Task\TaskFilters;
use App\Http\Dto\Requests\Task\TaskForm;
use App\Http\Dto\Responses\Task\TaskEditor;
use App\Http\Dto\Responses\Task\TaskModel;
use App\Http\Dto\Responses\Task\TasksCatalog;
use App\Models\Category;
use App\Models\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic;

class TaskService implements ITaskService
{
    public function createOrUpdateTask(TaskForm $form): void
    {
        /** @var Category $category */
        $category = Category::query()->findOrFail($form->categoryId);
        // Check that user really owns task category.
        Gate::authorize('update-or-delete-task-category', $category);
        $isCreating = is_null($form->id);
        if ($isCreating) {
            $task = new Task();
            $task->is_finished = false;
        } else {
            $task = Task::query()->findOrFail($form->id);
            Gate::authorize('update-or-delete-task', $task);
        }
        $task->priority = $form->priority;
        $task->description = $form->description;
        $task->category_id = $form->categoryId;
        if (isset($form->photo)) {
            if (!$isCreating && isset($task->photo_file_name)) {
                Storage::delete('task-photo-previews/' . $task->photo_file_name);
                Storage::delete('task-photos/' . $task->photo_file_name);
            }
            $newFileName = Str::random(32) . '.' . $form->photo->extension();
            $fullSizePath = $form->photo->storeAs('task-photos', $newFileName);
            if (!is_dir(Storage::path('task-photo-previews/'))) {
                mkdir(Storage::path('task-photo-previews/'));
            }
            ImageManagerStatic::make(Storage::path($fullSizePath))
                ->heighten(80)
                ->save(Storage::path("task-photo-previews/$newFileName"));
            $task->photo_file_name = $newFileName;
        } else {
            if ($isCreating) {
                $task->photo_file_name = null;
            }
        }
        $task->save();
        Log::info(
            (is_null($form->id)) ? 'Created task' : 'Updated task',
            ['id' => $task->id]
        );
    }

    public function getTasksCatalog(TaskFilters $filters): TasksCatalog
    {
        $query = Task::query()
            ->where('is_finished', $filters->onlyFinished)
            ->orderBy('priority', 'desc');
        if (isset($filters->categoryId)) {
            $query = $query->where('category_id', $filters->categoryId);
        }
        $tasks = $query
            ->get()
            ->filter(fn (Task $t) => $t->category->user->id == Auth::id())
            ->values();
        $result = new TasksCatalog();
        for ($i = 0; $i < count($tasks); $i++) {
            $columnNumber = $i % 2;
            /** @var Task $task */
            $task = $tasks[$i];
            $visibleDescription = $task->description;
            $hiddenDescription = null;
            if (mb_strlen($visibleDescription) > 250) {
                $visibleDescription = mb_substr($visibleDescription, 0, 250);
                $hiddenDescription = mb_substr($task->description, 250);
            }
            $photoUrl = null;
            $photoPreviewUrl = null;
            if (isset($task->photo_file_name)) {
                $photoUrl = Storage::url('task-photos/' . $task->photo_file_name);
                $photoPreviewUrl = Storage::url('task-photo-previews/' . $task->photo_file_name);
            }
            $result->columns[$columnNumber][] = new TaskModel(
                $task->id,
                $task->is_finished,
                $task->category->name,
                $task->category->font_color,
                $task->category->background_color,
                $task->priority,
                $visibleDescription,
                $hiddenDescription,
                $photoPreviewUrl,
                $photoUrl
            );
        }
        return $result;
    }

    public function getTaskEditor(int $id): TaskEditor
    {
        /** @var Task $task */
        $task = Task::query()->findOrFail($id);
        Gate::authorize('update-or-delete-task', $task);
        return new TaskEditor(
            $task->category_id,
            $task->priority,
            $task->description
        );
    }

    public function toggleTaskFinished(int $id): void
    {
        /** @var Task $task */
        $task = Task::query()->findOrFail($id);
        Gate::authorize('update-or-delete-task', $task);
        $task->is_finished = !($task->is_finished);
        $task->save();
    }

    public function deleteTask(int $id): void
    {
        /** @var Task $task */
        $task = Task::query()->findOrFail($id);
        Gate::authorize('update-or-delete-task', $task);
        if (isset($task->photo_file_name)) {
            Storage::delete('task-photo-previews/' . $task->photo_file_name);
            Storage::delete('task-photos/' . $task->photo_file_name);
        }
        $task->delete();
        Log::info('Deleted task', compact('id'));
    }

    public function deleteFinishedTasks(): void
    {
        Task::query()
            ->get()
            ->filter(fn (Task $t) => $t->category->user->id == Auth::id() && $t->is_finished)
            ->map(fn (Task $t) => $this->deleteTask($t->id));
        Log::info('Deleted all finished tasks', ['userId' =>  Auth::id()]);
    }
}
