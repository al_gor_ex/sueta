<?php

namespace App\Service\Task;

use App\Http\Dto\Requests\Task\CategoryForm;
use App\Http\Dto\Responses\Task\CategoriesList;
use App\Http\Dto\Responses\Task\CategoriesSelectList;
use App\Http\Dto\Responses\Task\CategoryEditor;
use App\Http\Dto\Responses\Task\CategoryModel;
use App\Http\Dto\Responses\Task\CategorySelectModel;
use App\Models\Category;
use App\Models\Enums\TaskPriority;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class CategoryService implements ICategoryService
{
    public function createOrUpdateCategory(CategoryForm $form): void
    {
        if (is_null($form->id)) {
            $category = new Category();
        } else {
            $category = Category::query()->findOrFail($form->id);
            Gate::authorize('update-or-delete-task-category', $category);
        }
        $category->name = $form->name;
        $category->background_color = $form->backgroundColor;
        $category->font_color = $this->getContrastColor($form->backgroundColor);
        $category->user_id = Auth::id();
        $category->save();
        Log::info(
            (is_null($form->id)) ? 'Created category' : 'Updated category',
            ['id' => $category->id]
        );
    }

    public function getCategoriesList(): CategoriesList
    {
        /** @var Category[] $categories */
        $categories = Category::query()
            ->where('user_id', Auth::id())
            ->orderBy('name')
            ->get();
        $result = new CategoriesList();
        foreach ($categories as $category) {
            $tasksCount = $category->tasks
                ->where('is_finished', false)
                ->count();
            $priorityStats = DB::table('tasks')
                ->where('category_id', $category->id)
                ->where('is_finished', false)
                ->groupBy('priority')
                ->selectRaw('priority, COUNT(*) AS count')
                ->orderBy('priority', 'desc')
                ->get();
            $tasksCountByPriority = [];
            // Initialize priorities array with zeros (to have all keys, not depending on values in db).
            foreach (TaskPriority::cases() as $priority) {
                $tasksCountByPriority[$priority->value] = 0;
            }
            foreach ($priorityStats as $stat) {
                $tasksCountByPriority[$stat->priority] = $stat->count;
            }
            $result->categories[] = new CategoryModel(
                $category->id,
                $category->name,
                $category->font_color,
                $category->background_color,
                $tasksCount,
                $tasksCountByPriority
            );
        }
        return $result;
    }

    public function getCategoriesSelectList(): CategoriesSelectList
    {
        $result = new CategoriesSelectList();
        Category::query()
            ->where('user_id', Auth::id())
            ->orderBy('name')
            ->get()
            ->map(fn (Category $c) => $result->categories[] = new CategorySelectModel(
                $c->id,
                $c->name,
                $c->font_color,
                $c->background_color
            ));
        return $result;
    }

    public function getCategoryEditor(int $id): CategoryEditor
    {
        /** @var Category $category */
        $category = Category::query()->findOrFail($id);
        Gate::authorize('update-or-delete-task-category', $category);
        return new CategoryEditor(
            $category->name,
            $category->background_color
        );
    }

    public function deleteCategory(int $id): void
    {
        /** @var Category $category */
        $category = Category::query()->findOrFail($id);
        Gate::authorize('update-or-delete-task-category', $category);
        /** @var ITaskService $taskService */
        $taskService = App::make(ITaskService::class);
        foreach ($category->tasks as $task) {
            $taskService->deleteTask($task->id);
        }
        $category->delete();
        Log::info('Deleted category', compact('id'));
    }

    private function getContrastColor(string $hexColor): string
    {
        /** @var int[] $rgb */
        $rgb = sscanf($hexColor, '#%02x%02x%02x');
        /** @var float[] $normRgb */
        $normRgb = collect($rgb)
            ->map(fn(int $component) => $component / 255)
            ->toArray();
        $gamma = 2.2;
        $luminance = 0.2126 * pow($normRgb[0], $gamma)
            + 0.7152 * pow($normRgb[1], $gamma)
            + 0.0722 * pow($normRgb[2], $gamma);
        return ($luminance > 0.3) ? '#000000' : '#ffffff';
    }
}
