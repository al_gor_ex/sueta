<?php

namespace App\Service\Task;

use App\Http\Dto\Requests\Task\CategoryForm;
use App\Http\Dto\Responses\Task\CategoriesList;
use App\Http\Dto\Responses\Task\CategoriesSelectList;
use App\Http\Dto\Responses\Task\CategoryEditor;

interface ICategoryService
{
    public function createOrUpdateCategory(CategoryForm $form): void;

    public function getCategoriesList(): CategoriesList;

    public function getCategoriesSelectList(): CategoriesSelectList;

    public function getCategoryEditor(int $id): CategoryEditor;

    public function deleteCategory(int $id): void;
}
