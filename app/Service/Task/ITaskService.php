<?php

namespace App\Service\Task;

use App\Http\Dto\Requests\Task\TaskFilters;
use App\Http\Dto\Requests\Task\TaskForm;
use App\Http\Dto\Responses\Task\TaskEditor;
use App\Http\Dto\Responses\Task\TasksCatalog;

interface ITaskService
{
    public function createOrUpdateTask(TaskForm $form): void;

    public function getTasksCatalog(TaskFilters $filters): TasksCatalog;

    public function getTaskEditor(int $id): TaskEditor;

    public function toggleTaskFinished(int $id): void;

    public function deleteTask(int $id): void;

    public function deleteFinishedTasks(): void;
}
