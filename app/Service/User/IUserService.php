<?php

namespace App\Service\User;

use App\Http\Dto\Requests\User\LoginForm;
use App\Http\Dto\Requests\User\RegistrationForm;

interface IUserService
{
    public function register(RegistrationForm $form): void;

    public function login(LoginForm $form): bool;
}
