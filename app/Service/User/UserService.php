<?php

namespace App\Service\User;

use App\Http\Dto\Requests\User\LoginForm;
use App\Http\Dto\Requests\User\RegistrationForm;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;

class UserService implements IUserService
{
    public function register(RegistrationForm $form): void
    {
        $user = new User();
        $user->name = $form->name;
        $user->email = $form->email;
        $user->password = Hash::make($form->password);
        $user->notifications_refresh_date = Carbon::now()->startOfDay();
        $user->save();
        Log::info('Created new user', ['id' => $user->id]);
        Auth::login($user);
    }

    public function login(LoginForm $form): bool
    {
        $result = Auth::attempt(
            [
                'email' => $form->email,
                'password' => $form->password
            ],
            $form->remember
        );
        if ($result) {
            Log::info('Login successful', ['userId' => Auth::id(), 'ip' => Request::ip()]);
        } else {
            Log::warning('Login failed', ['credentials' => $form, 'ip' => Request::ip()]);
        }
        return $result;
    }
}
