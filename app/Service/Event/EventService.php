<?php

namespace App\Service\Event;

use App\Http\Dto\Requests\Event\EventFilters;
use App\Http\Dto\Requests\Event\EventForm;
use App\Http\Dto\Responses\Event\EventEditor;
use App\Http\Dto\Responses\Event\EventModel;
use App\Http\Dto\Responses\Event\EventsList;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class EventService implements IEventService
{

    public function createOrUpdateEvent(EventForm $form): void
    {
        if (is_null($form->id)) {
            $event = new Event();
        } else {
            $event = Event::query()->findOrFail($form->id);
            Gate::authorize('update-or-delete-event', $event);
        }
        $event->description = $form->description;
        $event->starts_at = Carbon::createFromTimeString($form->startsAt);
        $event->user_id = Auth::id();
        $event->save();
        Log::info(
            (is_null($form->id)) ? 'Created event' : 'Updated event',
            ['id' => $event->id]
        );
    }

    public function getEventsList(?EventFilters $filters = null): EventsList
    {
        $query = Event::query()
            ->where('user_id', Auth::id())
            ->orderBy('starts_at');
        if (isset($filters)) {
            $query = $query->whereBetween(
                'starts_at',
                [
                    $filters->dateFrom->toDateTimeString(),
                    $filters->dateTo->toDateTimeString()
                ]
            );
        }
        $result = new EventsList();
        $query
            ->get()
            ->map(fn (Event $e) => $result->events[] = new EventModel(
                $e->id,
                $e->description,
                $e->starts_at->isoFormat('D MMM Y  H:mm')
            ));
        return $result;
    }

    public function getEventEditor(int $id): EventEditor
    {
        /** @var Event $event*/
        $event = Event::query()->findOrFail($id);
        Gate::authorize('update-or-delete-event', $event);
        return new EventEditor(
            $event->description,
            $event->starts_at->toDateTimeString()
        );
    }

    public function deleteEvent(int $id): void
    {
        /** @var Event $event*/
        $event = Event::query()->findOrFail($id);
        Gate::authorize('update-or-delete-event', $event);
        $event->delete();
        Log::info('Deleted event', compact('id'));
    }

    public function deletePastEvents(): void
    {
        $now = Carbon::now()->toDateTimeString();
        Event::query()
            ->where('user_id', Auth::id())
            ->where('starts_at', '<', $now)
            ->delete();
        Log::info('Deleted all past events', ['userId' =>  Auth::id()]);
    }
}
