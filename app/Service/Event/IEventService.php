<?php

namespace App\Service\Event;

use App\Http\Dto\Requests\Event\EventFilters;
use App\Http\Dto\Requests\Event\EventForm;
use App\Http\Dto\Responses\Event\EventEditor;
use App\Http\Dto\Responses\Event\EventsList;

interface IEventService
{
    public function createOrUpdateEvent(EventForm $form): void;

    public function getEventsList(?EventFilters $filters = null): EventsList;

    public function getEventEditor(int $id): EventEditor;

    public function deleteEvent(int $id): void;

    public function deletePastEvents(): void;
}
