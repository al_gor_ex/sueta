<?php

namespace App\Service\Calendar;

use App\Http\Dto\Requests\Calendar\CalendarFilters;
use App\Http\Dto\Responses\Calendar\CalendarResponse;

interface ICalendarService
{
    public function getCalendar(CalendarFilters $filters): CalendarResponse;
}
