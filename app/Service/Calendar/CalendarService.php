<?php

namespace App\Service\Calendar;

use App\Http\Dto\Requests\Calendar\CalendarFilters;
use App\Http\Dto\Responses\Calendar\CalendarCell;
use App\Http\Dto\Responses\Calendar\CalendarResponse;
use App\Http\Dto\Responses\Calendar\CellEvent;
use App\Models\Birthday;
use App\Models\Enums\EventType;
use App\Models\Event;
use App\Models\Routine;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CalendarService implements ICalendarService
{
    public function getCalendar(CalendarFilters $filters): CalendarResponse
    {
        $monthStart = Carbon::now()
            ->startOfMonth()
            ->addMonthsNoOverflow($filters->monthsDeltaFromNow)
            ->startOfMonth();
        $monthEnd = (clone $monthStart)->endOfMonth();

        $calendar = $this->initializeCalendar($monthStart);

        /** @var Event[] $events */
        $events = Event::query()
            ->where('user_id', Auth::id())
            ->whereBetween('starts_at', [$monthStart->toDateTimeString(), $monthEnd->toDateTimeString()])
            ->orderBy('starts_at')
            ->get();
        foreach ($events as $event) {
            $iRow = intdiv(
                (($monthStart->dayOfWeekIso - 2) + $event->starts_at->day),
                7
            );
            $iCol = $event->starts_at->dayOfWeekIso - 1;
            $calendar->cells[$iRow][$iCol]->events[] = new CellEvent(
                EventType::EVENT,
                $event->starts_at->isoFormat('H:mm') . ' ' . $event->description
            );
        }

        /** @var Birthday[] $birthdays */
        $birthdays = Birthday::query()
            ->where('user_id', Auth::id())
            ->get()
            ->filter(fn (Birthday $b) => $b->date->month == $monthStart->month)
            ->values();
        foreach ($birthdays as $birthday) {
            // Handle "29th of February" birthdays (they don't happen every year)
            if ($birthday->date->day > $monthStart->daysInMonth) {
                continue;
            }
            $iRow = intdiv(
                (($monthStart->dayOfWeekIso - 2) + $birthday->date->day),
                7
            );
            $iCol = (($monthStart->dayOfWeekIso - 2) + $birthday->date->day) % 7;
            $description = $birthday->person_name;
            if ($birthday->is_year_unknown == false) {
                $age = (clone $monthStart)
                    ->addDays($birthday->date->day)
                    ->diffInYears($birthday->date);
                $description = "$description ($age)";
            }
            $calendar->cells[$iRow][$iCol]->events[] = new CellEvent(
                EventType::BIRTHDAY,
                $description
            );
        }

        /** @var Routine[] $routines */
        $routines = Routine::query()
            ->where('user_id', Auth::id())
            ->get();
        $curDate = clone $monthStart;
        while (true) {
            foreach ($routines as $routine) {
                if ($curDate->diffInDays($routine->count_start_date) % $routine->period_length_days == 0) {
                    $iRow = intdiv(
                        (($monthStart->dayOfWeekIso - 2) + $curDate->day),
                        7
                    );
                    $iCol = $curDate->dayOfWeekIso - 1;
                    $calendar->cells[$iRow][$iCol]->events[] = new CellEvent(
                        EventType::ROUTINE,
                        $routine->description
                    );
                }
            }
            if ($curDate->day == $monthStart->daysInMonth) {
                break;
            }
            $curDate->addDay();
        }
        return $calendar;
    }

    private function initializeCalendar(Carbon $monthStart): CalendarResponse
    {
        $calendar = new CalendarResponse();
        $calendar->periodName = $monthStart->isoFormat('MMMM Y');
        // Capitalize the first letter of month name.
        $calendar->periodName = mb_convert_case($calendar->periodName, MB_CASE_TITLE, 'UTF-8');
        // Insert empty cells before numbered cells.
        $emptyStartCellsCount = $monthStart->dayOfWeekIso - 1;
        for ($i = 0; $i < $emptyStartCellsCount; $i++) {
            $calendar->cells[0][$i] = new CalendarCell();
        }
        // Insert current month's cells.
        $curDate = clone $monthStart;
        $i = 0;
        $iRow = 0;
        $iCol = $emptyStartCellsCount;
        while ($i != $monthStart->daysInMonth) {
            // If calendar's current line has ended, go to the start of the next line.
            if ($iCol > 6) {
                $iRow++;
                $iCol = 0;
                continue;
            }
            $calendar->cells[$iRow][$iCol] = new CalendarCell($curDate->day, $curDate->isCurrentDay());
            $i++;
            $curDate->addDay();
            $iCol++;
        }
        // Insert empty cells after numbered cells.
        while ($iCol < 7) {
            $calendar->cells[$iRow][$iCol] = new CalendarCell();
            $iCol++;
        }
        return $calendar;
    }
}
