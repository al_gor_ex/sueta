<?php

namespace App\Service\Birthday;

use App\Http\Dto\Requests\Birthday\BirthdayFilters;
use App\Http\Dto\Requests\Birthday\BirthdayForm;
use App\Http\Dto\Responses\Birthday\BirthdayEditor;
use App\Http\Dto\Responses\Birthday\BirthdaysList;

interface IBirthdayService
{
    public function createOrUpdateBirthday(BirthdayForm $form): void;

    public function getBirthdaysList(BirthdayFilters $filters): BirthdaysList;

    public function getBirthdayEditor(int $id): BirthdayEditor;

    public function deleteBirthday(int $id): void;
}
