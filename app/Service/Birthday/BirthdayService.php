<?php

namespace App\Service\Birthday;

use App\Http\Dto\Requests\Birthday\BirthdayFilters;
use App\Http\Dto\Requests\Birthday\BirthdayForm;
use App\Http\Dto\Responses\Birthday\BirthdayEditor;
use App\Http\Dto\Responses\Birthday\BirthdayModel;
use App\Http\Dto\Responses\Birthday\BirthdaysList;
use App\Models\Birthday;
use App\Models\Enums\BirthdaySorting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;

class BirthdayService implements IBirthdayService
{
    public function createOrUpdateBirthday(BirthdayForm $form): void
    {
        $isCreating = is_null($form->id);
        if ($isCreating) {
            $birthday = new Birthday();
        } else {
            $birthday = Birthday::query()->findOrFail($form->id);
            Gate::authorize('update-or-delete-birthday', $birthday);
        }
        $birthday->person_name = $form->personName;
        $birthday->date = Carbon::createFromIsoFormat('Y-MM-DD', $form->date)->startOfDay();
        $birthday->is_year_unknown = $form->isYearUnknown;
        $birthday->user_id = Auth::id();
        if (isset($form->personPhoto)) {
            if ($isCreating == false && isset($birthday->person_photo_path)) {
                Storage::delete($birthday->person_photo_path);
            }
            $path = $form->personPhoto->store('birthday-person-photos');
            ImageManagerStatic::make(Storage::path($path))
                ->heighten(100)
                ->save();
            $birthday->person_photo_path = $path;
        } else {
            if ($isCreating) {
                $birthday->person_photo_path = null;
            }
        }
        $birthday->save();
        Log::info(
            (is_null($form->id)) ? 'Created birthday' : 'Updated birthday',
            ['id' => $birthday->id]
        );
    }

    public function getBirthdaysList(BirthdayFilters $filters): BirthdaysList
    {
        $birthdays = Birthday::query()
            ->where('user_id', Auth::id())
            ->get();
        switch ($filters->sortBy) {
            case BirthdaySorting::PERSON_NAME:
                $birthdays = $birthdays->sortBy('person_name')->values();
                break;
            case BirthdaySorting::DAY_AND_MONTH:
                $birthdays = $birthdays->sortBy([
                    // First sort by month, then by day
                    fn(Birthday $a, Birthday $b) => $a->date->month <=> $b->date->month,
                    fn(Birthday $a, Birthday $b) => $a->date->day <=> $b->date->day
                ])->values();
                break;
        }
        $result = new BirthdaysList();
        /** @var Birthday[] $birthdays */
        foreach ($birthdays as $birthday) {
            $personPhotoUrl = null;
            if (isset($birthday->person_photo_path)) {
                $personPhotoUrl = Storage::url($birthday->person_photo_path);
            }
            $dateFormat = $birthday->is_year_unknown ? 'D MMM' : 'D MMM Y';
            $date = $birthday->date->isoFormat($dateFormat);
            $age = null;
            if ($birthday->is_year_unknown == false) {
                $age = $birthday->date->age;
            }
            $result->birthdays[] = new BirthdayModel(
                $birthday->id,
                $birthday->person_name,
                $personPhotoUrl,
                $date,
                $age
            );
        }
        return $result;
    }

    public function getBirthdayEditor(int $id): BirthdayEditor
    {
        /** @var Birthday $birthday */
        $birthday = Birthday::query()->findOrFail($id);
        Gate::authorize('update-or-delete-birthday', $birthday);
        return new BirthdayEditor(
            $birthday->person_name,
            $birthday->date->toDateString(),
            $birthday->is_year_unknown
        );
    }

    public function deleteBirthday(int $id): void
    {
        /** @var Birthday $birthday */
        $birthday = Birthday::query()->findOrFail($id);
        Gate::authorize('update-or-delete-birthday', $birthday);
        if (isset($birthday->person_photo_path)) {
            Storage::delete($birthday->person_photo_path);
        }
        $birthday->delete();
        Log::info('Deleted birthday', compact('id'));
    }
}
