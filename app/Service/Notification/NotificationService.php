<?php

namespace App\Service\Notification;

use App\Http\Dto\Requests\Notification\NotificationFilters;
use App\Http\Dto\Responses\Notification\NotificationModel;
use App\Http\Dto\Responses\Notification\NotificationsBatch;
use App\Http\Dto\Responses\Notification\NotificationsList;
use App\Models\Birthday;
use App\Models\Enums\EventType;
use App\Models\Event;
use App\Models\Notification;
use App\Models\Routine;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class NotificationService implements INotificationService
{

    public function getNotificationsList(NotificationFilters $filters): NotificationsList
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user->notifications_refresh_date < now()->startOfDay()) {
            $this->refreshNotifications($user);
        }
        $curDate = now()->startOfDay();
        /** @var Notification[]|Collection $notifications */
        $notifications = Notification::query()
            ->where('user_id', $user->id)
            ->get();
        $pendingCount = $notifications
            ->filter(fn(Notification $n) => $n->is_confirmed == false)
            ->count();
        $outdatedCount = $notifications
            ->filter(fn(Notification $n) => $n->is_confirmed == false && $n->created_at < $curDate)
            ->count();
        /** @var Notification[][]|Collection $notificationGroups */
        $notificationGroups = $notifications->groupBy('created_at')->sortKeys(descending: true);
        $totalPages = (int)ceil($notificationGroups->count() / $filters->pageSize);
        if ($filters->currentPage > $totalPages) {
            $filters->currentPage = 1;
        }
        $result = new NotificationsList($pendingCount, $outdatedCount, $filters->currentPage, $totalPages);
        /** @var Notification[][]|Collection $notificationGroups */
        $notificationGroups = $notificationGroups->forPage($filters->currentPage, $filters->pageSize);
        $i = 0;
        /** @var string $date */
        /** @var Notification[]|Collection $dateGroup */
        foreach ($notificationGroups as $date => $dateGroup) {
            $groupDate = Carbon::createFromTimeString($date);
            $title = $groupDate->isoFormat('D MMMM');
            $title .= match ($curDate->diffInDays($groupDate)) {
                0 => ' (сегодня)',
                1 => ' (вчера)',
                2 => ' (позавчера)',
                default => ''
            };
            $hasNonConfirmedNotifications = $dateGroup->filter(fn(Notification $n) => !$n->is_confirmed)->count() > 0;
            $hasPendingNotifications = false;
            $hasOutdatedNotifications = false;
            if ($hasNonConfirmedNotifications) {
                if ($groupDate < $curDate) {
                    $hasOutdatedNotifications = true;
                } else {
                    $hasPendingNotifications = true;
                }
            }
            $result->notificationBatches[$i] = new NotificationsBatch(
                $title,
                $hasPendingNotifications,
                $hasOutdatedNotifications
            );
            $dateGroup->map(
                fn(Notification $n) => $result->notificationBatches[$i]->notifications[] = new NotificationModel(
                    $n->id,
                    $n->event_type,
                    $n->description,
                    $n->is_confirmed,
                    isset($n->photo_path) ? Storage::url($n->photo_path) : null
                )
            );
            $i++;
        }
        return $result;
    }

    public function confirmNotification(int $id): void
    {
        /** @var Notification $notification */
        $notification = Notification::query()->findOrFail($id);
        Gate::authorize('confirm-notification', $notification);
        $notification->is_confirmed = true;
        $notification->save();
        Log::info('Confirmed notification', ['notificationId' => $id, 'userId' => Auth::id()]);
    }

    public function clearNotifications(): void
    {
        /** @var Notification[] $notifications */
        $notifications = Notification::query()
            ->where('user_id', Auth::id())
            ->get();
        foreach ($notifications as $notification) {
            if (isset($notification->photo_path)) {
                Storage::delete($notification->photo_path);
            }
            $notification->delete();
        }
        Log::info('Cleared notifications', ['userId' => Auth::id()]);
    }

    private function refreshNotifications(User $user): void
    {
        /** @var Event[]|Collection $events */
        $events = Event::query()->where('user_id', $user->id)->orderBy('starts_at')->get();
        /** @var Birthday[]|Collection $birthdays */
        $birthdays = Birthday::query()->where('user_id', $user->id)->get();
        /** @var Routine[]|Collection $routines */
        $routines = Routine::query()->where('user_id', $user->id)->get();

        $curDate = $user->notifications_refresh_date->addDay();
        $finishDate = now()->startOfDay();

        while (true) {

            $curDateString = $curDate->toDateString();
            /** @var Event[] $curEvents */
            $curEvents = $events->filter(fn(Event $e) => $e->starts_at->toDateString() == $curDateString);
            foreach ($curEvents as $event) {
                $notification = new Notification();
                $notification->description = $event->starts_at->isoFormat('HH:mm') . ' ' . $event->description;
                $notification->event_type = EventType::EVENT;
                $notification->is_confirmed = false;
                $notification->user_id = $user->id;
                $notification->created_at = $curDate;
                $notification->save();
            }

            /** @var Birthday[] $curBirthdays */
            $curBirthdays = $birthdays->filter(
                fn(Birthday $b) => ($b->date->month == $curDate->month && $b->date->day == $curDate->day)
            );
            foreach ($curBirthdays as $birthday) {
                $notification = new Notification();
                $notification->description = $birthday->person_name;
                if ($birthday->is_year_unknown == false) {
                    $age = $curDate->diffInYears($birthday->date);
                    $notification->description = "$notification->description ($age)";
                }
                $notification->event_type = EventType::BIRTHDAY;
                $notification->is_confirmed = false;
                if (isset($birthday->person_photo_path)) {
                    $copyPath = Str::of($birthday->person_photo_path)
                        ->replace('birthday-person-photos', 'notification-photos');
                    Storage::copy($birthday->person_photo_path, $copyPath);
                    $notification->photo_path = $copyPath;
                }
                $notification->user_id = $user->id;
                $notification->created_at = $curDate;
                $notification->save();
            }

            /** @var Routine[] $curRoutines */
            $curRoutines = $routines->filter(
                fn(Routine $r) => $curDate->diffInDays($r->count_start_date) % $r->period_length_days == 0
            );
            foreach ($curRoutines as $routine) {
                $notification = new Notification();
                $notification->description = $routine->description;
                $notification->event_type = EventType::ROUTINE;
                $notification->is_confirmed = false;
                $notification->user_id = $user->id;
                $notification->created_at = $curDate;
                $notification->save();
            }

            if ($curDate == $finishDate) {
                break;
            }
            $curDate->addDay();
        }

        $user->notifications_refresh_date = $finishDate;
        $user->save();
    }
}
