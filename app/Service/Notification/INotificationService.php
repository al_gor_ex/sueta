<?php

namespace App\Service\Notification;

use App\Http\Dto\Requests\Notification\NotificationFilters;
use App\Http\Dto\Responses\Notification\NotificationsList;

interface INotificationService
{
    public function getNotificationsList(NotificationFilters $filters): NotificationsList;

    public function confirmNotification(int $id): void;

    public function clearNotifications(): void;
}
