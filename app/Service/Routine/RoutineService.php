<?php

namespace App\Service\Routine;

use App\Http\Dto\Requests\Routine\RoutineForm;
use App\Http\Dto\Responses\Routine\RoutineEditor;
use App\Http\Dto\Responses\Routine\RoutineModel;
use App\Http\Dto\Responses\Routine\RoutinesList;
use App\Models\Routine;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class RoutineService implements IRoutineService
{

    public function createOrUpdateRoutine(RoutineForm $form): void
    {
        if (is_null($form->id)) {
            $routine = new Routine();
        } else {
            $routine = Routine::query()->findOrFail($form->id);
            Gate::authorize('update-or-delete-routine', $routine);
        }
        $routine->user_id = Auth::id();
        $routine->description = $form->description;
        $routine->count_start_date = Carbon::createFromIsoFormat('Y-MM-DD', $form->countStartDate)->startOfDay();
        $routine->period_length_days = $form->periodLengthDays;
        $routine->save();
        Log::info(
            (is_null($form->id)) ? 'Created routine' : 'Updated routine',
            ['id' => $routine->id]
        );
    }

    public function getRoutinesList(): RoutinesList
    {
        $result = new RoutinesList();
        Routine::query()
            ->where('user_id', Auth::id())
            ->orderBy('period_length_days')
            ->get()
            ->map(fn (Routine $r) => $result->routines[] = new RoutineModel(
                $r->id,
                $r->description,
                $r->count_start_date->isoFormat('D MMM Y'),
                $r->period_length_days
            ));
        return $result;
    }

    public function getRoutineEditor(int $id): RoutineEditor
    {
        /** @var Routine $routine */
        $routine = Routine::query()->findOrFail($id);
        Gate::authorize('update-or-delete-routine', $routine);
        return new RoutineEditor(
            $routine->description,
            $routine->count_start_date->toDateString(),
            $routine->period_length_days
        );
    }

    public function deleteRoutine(int $id): void
    {
        /** @var Routine $routine */
        $routine = Routine::query()->findOrFail($id);
        Gate::authorize('update-or-delete-routine', $routine);
        $routine->delete();
        Log::info('Deleted routine', compact('id'));
    }
}
