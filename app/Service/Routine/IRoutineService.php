<?php

namespace App\Service\Routine;

use App\Http\Dto\Requests\Routine\RoutineForm;
use App\Http\Dto\Responses\Routine\RoutineEditor;
use App\Http\Dto\Responses\Routine\RoutinesList;

interface IRoutineService
{
    public function createOrUpdateRoutine(RoutineForm $form): void;

    public function getRoutinesList(): RoutinesList;

    public function getRoutineEditor(int $id): RoutineEditor;

    public function deleteRoutine(int $id): void;
}
