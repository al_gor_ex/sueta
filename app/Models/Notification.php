<?php

namespace App\Models;

use App\Models\Enums\EventType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $description
 * @property EventType $event_type
 * @property boolean $is_confirmed
 * @property Carbon|null $created_at
 * @property string|null $photo_path
 * @property int $user_id
 * @property-read User $user
 */
class Notification extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'event_type' => EventType::class,
        'created_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
