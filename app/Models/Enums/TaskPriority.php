<?php

namespace App\Models\Enums;

enum TaskPriority: int
{
    case HIGH = 3;
    case MEDIUM = 2;
    case LOW = 1;
}
