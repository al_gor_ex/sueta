<?php

namespace App\Models\Enums;

enum BirthdaySorting: string
{
    case PERSON_NAME = 'person-name';
    case DAY_AND_MONTH = 'day-and-month';
}
