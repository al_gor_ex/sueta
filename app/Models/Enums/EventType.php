<?php

namespace App\Models\Enums;

enum EventType: string
{
    case BIRTHDAY = 'birthday';
    case EVENT = 'event';
    case ROUTINE = 'routine';
}
