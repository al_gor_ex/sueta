<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $description
 * @property Carbon $starts_at
 * @property int $user_id
 * @property-read User $user
 */
class Event extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'starts_at' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
