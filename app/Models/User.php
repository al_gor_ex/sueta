<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;

/**
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon $notifications_refresh_date
 * @property-read Collection|Birthday[] $birthdays
 * @property-read Collection|Category[] $categories
 * @property-read Collection|Event[] $events
 * @property-read Collection|Notification[] $notifications
 * @property-read Collection|Routine[] $routines
 * @property-read Collection|PersonalAccessToken[] $tokens
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'notifications_refresh_date' => 'datetime'
    ];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

    public function routines()
    {
        return $this->hasMany(Routine::class);
    }

    public function events()
    {
        return $this->hasMany(Event::class);
    }

    public function birthdays()
    {
        return $this->hasMany(Birthday::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class);
    }
}
