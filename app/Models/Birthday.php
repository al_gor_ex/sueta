<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $person_name
 * @property string|null $person_photo_path
 * @property Carbon $date
 * @property boolean $is_year_unknown
 * @property int $user_id
 * @property-read User $user
 */
class Birthday extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'date' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
