<?php

namespace App\Models;

use App\Models\Enums\TaskPriority;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property TaskPriority $priority
 * @property string $description
 * @property string|null $photo_file_name
 * @property boolean $is_finished
 * @property int $category_id
 * @property-read Category $category
 */
class Task extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'priority' => TaskPriority::class
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
