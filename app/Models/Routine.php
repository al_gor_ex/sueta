<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $description
 * @property Carbon $count_start_date
 * @property int $period_length_days
 * @property int $user_id
 * @property-read User $user
 */
class Routine extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $casts = [
        'count_start_date' => 'datetime'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
