<?php

namespace App\Http\Dto\Requests\Calendar;

class CalendarFilters
{
    public function __construct(
        public int $monthsDeltaFromNow
    )
    {
    }
}
