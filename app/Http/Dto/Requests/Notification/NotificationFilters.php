<?php

namespace App\Http\Dto\Requests\Notification;

class NotificationFilters
{
    public function __construct(
        public int $currentPage,
        public int $pageSize = 3
    )
    {
    }
}
