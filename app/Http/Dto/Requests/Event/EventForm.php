<?php

namespace App\Http\Dto\Requests\Event;

class EventForm
{
    public function __construct(
        public ?int $id,
        public string $description,
        public string $startsAt
    )
    {
    }
}
