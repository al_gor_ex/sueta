<?php

namespace App\Http\Dto\Requests\Event;

use Carbon\Carbon;

class EventFilters
{
    public function __construct(
        public Carbon $dateFrom,
        public Carbon $dateTo
    )
    {
    }
}
