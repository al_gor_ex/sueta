<?php

namespace App\Http\Dto\Requests\Routine;

class RoutineForm
{
    public function __construct(
        public ?int $id,
        public string $description,
        public string $countStartDate,
        public int $periodLengthDays
    )
    {
    }
}
