<?php

namespace App\Http\Dto\Requests\Task;

class TaskFilters
{
    public function __construct(
        public bool $onlyFinished,
        public ?int $categoryId
    )
    {
    }
}
