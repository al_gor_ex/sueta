<?php

namespace App\Http\Dto\Requests\Task;

use App\Models\Enums\TaskPriority;
use Illuminate\Http\UploadedFile;

class TaskForm
{
    public function __construct(
        public ?int $id,
        public int $categoryId,
        public TaskPriority $priority,
        public string $description,
        public ?UploadedFile $photo
    )
    {
    }
}
