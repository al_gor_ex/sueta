<?php

namespace App\Http\Dto\Requests\Task;

class CategoryForm
{
    public function __construct(
        public ?int $id,
        public string $name,
        public string $backgroundColor
    )
    {
    }
}
