<?php

namespace App\Http\Dto\Requests\User;

class RegistrationForm
{
    public function __construct(
        public string $name,
        public string $email,
        public string $password
    )
    {
    }
}
