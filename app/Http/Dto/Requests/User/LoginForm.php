<?php

namespace App\Http\Dto\Requests\User;

class LoginForm
{
    public function __construct(
        public string $email,
        public string $password,
        public bool $remember
    )
    {
    }
}
