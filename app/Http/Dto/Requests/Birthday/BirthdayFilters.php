<?php

namespace App\Http\Dto\Requests\Birthday;

use App\Models\Enums\BirthdaySorting;

class BirthdayFilters
{
    public function __construct(
        public BirthdaySorting $sortBy
    )
    {
    }
}
