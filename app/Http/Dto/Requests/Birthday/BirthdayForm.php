<?php

namespace App\Http\Dto\Requests\Birthday;

use Illuminate\Http\UploadedFile;

class BirthdayForm
{
    public function __construct(
        public ?int $id,
        public string $personName,
        public ?UploadedFile $personPhoto,
        public string $date,
        public bool $isYearUnknown
    )
    {
    }
}
