<?php

namespace App\Http\Dto\Responses\Calendar;

class CalendarResponse
{
    public string $periodName;
    /** @var CalendarCell[][] */
    public array $cells = [];
}
