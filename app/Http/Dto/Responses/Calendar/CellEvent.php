<?php

namespace App\Http\Dto\Responses\Calendar;

use App\Models\Enums\EventType;

class CellEvent
{
    public function __construct(
        public EventType $type,
        public string $description
    )
    {
    }
}
