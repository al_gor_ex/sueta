<?php

namespace App\Http\Dto\Responses\Calendar;

class CalendarCell
{
    /** @var CellEvent[] $events */
    public array $events = [];

    public function __construct(
        public ?int $dayNumber = null,
        public bool $isCurrentDate = false
    )
    {
    }
}
