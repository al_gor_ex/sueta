<?php

namespace App\Http\Dto\Responses\Routine;

class RoutineEditor
{
    public function __construct(
        public string $description,
        public string $countStartDate,
        public int $periodLengthDays
    )
    {
    }
}
