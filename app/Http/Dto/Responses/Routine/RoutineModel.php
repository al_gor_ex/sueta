<?php

namespace App\Http\Dto\Responses\Routine;

class RoutineModel
{
    public function __construct(
        public int $id,
        public string $description,
        public string $countStartDate,
        public int $periodLengthDays
    )
    {
    }
}
