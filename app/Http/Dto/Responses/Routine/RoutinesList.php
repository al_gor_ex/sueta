<?php

namespace App\Http\Dto\Responses\Routine;

class RoutinesList
{
    /** @var RoutineModel[] $routines */
    public array $routines = [];
}
