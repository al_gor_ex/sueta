<?php

namespace App\Http\Dto\Responses\Task;

class CategoryEditor
{
    public function __construct(
        public string $name,
        public string $backgroundColor
    )
    {
    }
}
