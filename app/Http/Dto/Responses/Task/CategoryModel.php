<?php

namespace App\Http\Dto\Responses\Task;

class CategoryModel
{
    /** @var int[] $tasksCountByPriority */
    public array $tasksCountByPriority;

    /** @param int[] $tasksCountByPriority */
    public function __construct(
        public int $id,
        public string $name,
        public string $fontColor,
        public string $backgroundColor,
        public int $tasksCount,
        array $tasksCountByPriority
    )
    {
        $this->tasksCountByPriority = $tasksCountByPriority;
    }
}
