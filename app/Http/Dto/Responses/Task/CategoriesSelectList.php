<?php

namespace App\Http\Dto\Responses\Task;

class CategoriesSelectList
{
    /** @var CategorySelectModel[] $categories */
    public array $categories = [];
}
