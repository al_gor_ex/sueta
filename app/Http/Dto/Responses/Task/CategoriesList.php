<?php

namespace App\Http\Dto\Responses\Task;

class CategoriesList
{
    /** @var CategoryModel[] $categories */
    public array $categories = [];
}
