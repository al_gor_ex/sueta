<?php

namespace App\Http\Dto\Responses\Task;

class TasksCatalog
{
    /** @var TaskModel[][] $columns */
    public array $columns = [];
}
