<?php

namespace App\Http\Dto\Responses\Task;

use App\Models\Enums\TaskPriority;

class TaskEditor
{
    public function __construct(
        public int $categoryId,
        public TaskPriority $priority,
        public string $description
    )
    {
    }
}
