<?php

namespace App\Http\Dto\Responses\Task;

use App\Models\Enums\TaskPriority;

class TaskModel
{
    public function __construct(
        public int $id,
        public bool $isFinished,
        public string $categoryName,
        public string $categoryFontColor,
        public string $categoryBackgroundColor,
        public TaskPriority $priority,
        public string $visibleDescription,
        public ?string $hiddenDescription,
        public ?string $photoPreviewUrl,
        public ?string $photoFullSizeUrl
    )
    {
    }
}
