<?php

namespace App\Http\Dto\Responses\Task;

class CategorySelectModel
{
    public function __construct(
        public int $id,
        public string $name,
        public string $fontColor,
        public string $backgroundColor
    )
    {
    }
}
