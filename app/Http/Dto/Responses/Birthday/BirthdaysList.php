<?php

namespace App\Http\Dto\Responses\Birthday;

class BirthdaysList
{
    /** @var BirthdayModel[] $birthdays */
    public array $birthdays = [];
}
