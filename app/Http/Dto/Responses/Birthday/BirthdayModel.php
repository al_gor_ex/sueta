<?php

namespace App\Http\Dto\Responses\Birthday;

class BirthdayModel
{
    public function __construct(
        public int $id,
        public string $personName,
        public ?string $photoUrl,
        public string $date,
        public ?int $age
    )
    {
    }
}
