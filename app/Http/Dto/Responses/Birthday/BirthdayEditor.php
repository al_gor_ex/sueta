<?php

namespace App\Http\Dto\Responses\Birthday;

class BirthdayEditor
{
    public function __construct(
        public string $personName,
        public string $date,
        public bool $isYearUnknown
    )
    {
    }
}
