<?php

namespace App\Http\Dto\Responses\Notification;

class NotificationsBatch
{
    /** @var NotificationModel[] $notifications */
    public array $notifications = [];

    public function __construct(
        public string $title,
        public bool $hasPendingNotifications,
        public bool $hasOutdatedNotifications
    )
    {
    }
}
