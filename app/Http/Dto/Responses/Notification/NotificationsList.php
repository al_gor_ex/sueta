<?php

namespace App\Http\Dto\Responses\Notification;

class NotificationsList
{
    /** @var NotificationsBatch[] $notificationBatches */
    public array $notificationBatches = [];

    public function __construct(
        public int $pendingCount,
        public int $outdatedCount,
        public int $currentPage,
        public int $totalPages
    )
    {
    }
}
