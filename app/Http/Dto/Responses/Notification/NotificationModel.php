<?php

namespace App\Http\Dto\Responses\Notification;

use App\Models\Enums\EventType;

class NotificationModel
{
    public function __construct(
        public int $id,
        public EventType $eventType,
        public string $description,
        public bool $isConfirmed,
        public ?string $photoUrl
    )
    {
    }
}
