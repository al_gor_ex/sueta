<?php

namespace App\Http\Dto\Responses\Event;

class EventEditor
{
    public function __construct(
        public string $description,
        public string $startsAt
    )
    {
    }
}
