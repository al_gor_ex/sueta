<?php

namespace App\Http\Dto\Responses\Event;

class EventsList
{
    /** @var EventModel[] $events */
    public array $events = [];
}
