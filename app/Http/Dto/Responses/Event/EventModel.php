<?php

namespace App\Http\Dto\Responses\Event;

class EventModel
{
    public function __construct(
        public int $id,
        public string $description,
        public string $startsAt
    )
    {
    }
}
