<?php

namespace App\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class EncryptCookies extends Middleware
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array<int, string>
     */
    protected $except = [
        'birthdayFilters_sortBy',
        'eventFilters_dateFrom',
        'eventFilters_dateTo',
        'taskFilters_onlyFinished',
        'taskFilters_categoryId'
    ];
}
