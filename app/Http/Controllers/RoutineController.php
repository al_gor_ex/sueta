<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Routine\RoutineForm;
use App\Service\Routine\IRoutineService;
use Illuminate\Http\Request;

class RoutineController extends Controller
{
    public function __construct(
        private IRoutineService $routineService
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $result = $this->routineService->getRoutinesList();
        return view('registry.routines.routines-list', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('registry.routines.new-edit-routine', ['id' => -1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|string|max:255|min:1',
            'countStartDate' => 'required|date',
            'periodLengthDays' => 'required|integer|min:1'
        ]);
        $this->routineService->createOrUpdateRoutine(
            new RoutineForm(
                null,
                $request->description,
                $request->countStartDate,
                $request->periodLengthDays
            )
        );
        return redirect()->route('routines.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $routine = $this->routineService->getRoutineEditor($id);
        return view('registry.routines.new-edit-routine', compact('id', 'routine'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required|string|max:255|min:1',
            'countStartDate' => 'required|date',
            'periodLengthDays' => 'required|integer|min:1'
        ]);
        $this->routineService->createOrUpdateRoutine(
            new RoutineForm(
                $id,
                $request->description,
                $request->countStartDate,
                $request->periodLengthDays
            )
        );
        return redirect()->route('routines.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->routineService->deleteRoutine($id);
        return redirect()->back();
    }
}
