<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Calendar\CalendarFilters;
use App\Service\Calendar\ICalendarService;
use Illuminate\Http\Request;

class CalendarController extends Controller
{
    public function __construct(
        private ICalendarService $calendarService
    )
    {
    }

    public function index(Request $request)
    {
        $calendar = $this->calendarService->getCalendar(
            new CalendarFilters($request->query('monthsDeltaFromNow', 0))
        );
        return view('dashboard.calendar', compact('calendar'));
    }
}
