<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\User\LoginForm;
use App\Http\Dto\Requests\User\RegistrationForm;
use App\Providers\RouteServiceProvider;
use App\Service\User\IUserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rules\Password;

class UserController extends Controller
{
    public function __construct(
        private IUserService $userService
    )
    {
    }

    public function create()
    {
        return view('auth.registration');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Password::min(8)->letters()->numbers()->symbols()]
        ]);
        $this->userService->register(
            new RegistrationForm($request->name, $request->email, $request->password)
        );
        return redirect(RouteServiceProvider::HOME);
    }

    public function login(Request $request)
    {
        $isSuccessful = $this->userService->login(
            new LoginForm($request->email, $request->password, $request->boolean('remember'))
        );
        if ($isSuccessful) {
            $request->session()->regenerate();
            return redirect(RouteServiceProvider::HOME);
        } else {
            $request->session()->flash('failed');
            return back()->withInput();
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login');
    }
}
