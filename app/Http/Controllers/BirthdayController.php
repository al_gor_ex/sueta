<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Birthday\BirthdayFilters;
use App\Http\Dto\Requests\Birthday\BirthdayForm;
use App\Models\Enums\BirthdaySorting;
use App\Service\Birthday\IBirthdayService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\File;

class BirthdayController extends Controller
{
    public function __construct(
        private IBirthdayService $birthdayService
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $sortBy = BirthdaySorting::PERSON_NAME;
        if ($request->hasCookie('birthdayFilters_sortBy')) {
            $sortBy = BirthdaySorting::from($request->cookie('birthdayFilters_sortBy'));
        }
        $result = $this->birthdayService->getBirthdaysList(
            new BirthdayFilters($sortBy)
        );
        return view('registry.birthdays.birthdays-list', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('registry.birthdays.new-edit-birthday', ['id' => -1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'personName' => 'required|string|max:255|min:1',
            'personPhoto' => ['sometimes', File::image()->max(10 * 1024)],
            'isYearUnknown' => 'sometimes|accepted',
            'date' => 'exclude_if:isYearUnknown,on|date|before:now'
        ]);
        $this->birthdayService->createOrUpdateBirthday(
            new BirthdayForm(
                null,
                $request->personName,
                $request->file('personPhoto'),
                $request->date,
                $request->boolean('isYearUnknown')
            )
        );
        return redirect()->route('birthdays.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $birthday = $this->birthdayService->getBirthdayEditor($id);
        return view('registry.birthdays.new-edit-birthday', compact('id', 'birthday'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'personName' => 'required|string|max:255|min:1',
            'personPhoto' => ['sometimes', File::image()->max(10 * 1024)],
            'isYearUnknown' => 'sometimes|accepted',
            'date' => 'exclude_if:isYearUnknown,on|date|before:now'
        ]);
        $this->birthdayService->createOrUpdateBirthday(
            new BirthdayForm(
                $id,
                $request->personName,
                $request->file('personPhoto'),
                $request->date,
                $request->boolean('isYearUnknown')
            )
        );
        return redirect()->route('birthdays.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->birthdayService->deleteBirthday($id);
        return redirect()->back();
    }
}
