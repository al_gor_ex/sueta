<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Task\CategoryForm;
use App\Service\Task\ICategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function __construct(
        private ICategoryService $categoryService
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $result = $this->categoryService->getCategoriesList();
        return view('registry.tasks.task-categories-list', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('registry.tasks.new-edit-task-category', ['id' => -1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255|min:1',
            'backgroundColor' => ['required', 'string', 'regex:/#[0-9a-f]{6}/']
        ]);
        $this->categoryService->createOrUpdateCategory(
            new CategoryForm(null, $request->name, $request->backgroundColor)
        );
        return redirect()->route('categories.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $category = $this->categoryService->getCategoryEditor($id);
        return view('registry.tasks.new-edit-task-category', compact('id', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255|min:1',
            'backgroundColor' => ['required', 'string', 'regex:/#[0-9a-f]{6}/']
        ]);
        $this->categoryService->createOrUpdateCategory(
            new CategoryForm($id, $request->name, $request->backgroundColor)
        );
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->categoryService->deleteCategory($id);
        return redirect()->route('categories.index');
    }
}
