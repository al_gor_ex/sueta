<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Task\TaskFilters;
use App\Http\Dto\Requests\Task\TaskForm;
use App\Models\Birthday;
use App\Models\Enums\TaskPriority;
use App\Models\Task;
use App\Service\Task\ICategoryService;
use App\Service\Task\ITaskService;
use Illuminate\Http\Request;
use Illuminate\Support\Benchmark;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Validation\Rules\File;

class TaskController extends Controller
{
    public function __construct(
        private ITaskService $taskService,
        private ICategoryService $categoryService
    )
    {
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $onlyFinished = false;
        $categoryId = null;
        if ($request->hasCookie('taskFilters_onlyFinished')) {
            $onlyFinished = $request->cookie('taskFilters_onlyFinished');
        }
        if ($request->hasCookie('taskFilters_categoryId') && $request->cookie('taskFilters_categoryId') !== 'null') {
            $categoryId = $request->cookie('taskFilters_categoryId');
        }
        $catalog = $this->taskService->getTasksCatalog(
            new TaskFilters($onlyFinished, $categoryId)
        );
        $categoriesList = $this->categoryService->getCategoriesSelectList();
        return view('registry.tasks.tasks-list', compact('catalog', 'categoriesList'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categoriesList = $this->categoryService->getCategoriesSelectList();
        return view('registry.tasks.new-edit-task', ['id' => -1, ...compact('categoriesList')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'categoryId' => 'required|int|exists:categories,id',
            'priority' => ['required', new Enum(TaskPriority::class)],
            'description' => 'required|string|max:2048|min:1',
            'photo' => ['sometimes', File::image()->max(10 * 1024)],
        ]);
        $this->taskService->createOrUpdateTask(
            new TaskForm(
                null,
                $request->categoryId,
                TaskPriority::from($request->priority),
                $request->description,
                $request->file('photo')
            )
        );
        return redirect()->route('tasks.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $task = $this->taskService->getTaskEditor($id);
        $categoriesList = $this->categoryService->getCategoriesSelectList();
        return view('registry.tasks.new-edit-task', compact('id', 'task', 'categoriesList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'categoryId' => 'required|int|exists:categories,id',
            'priority' => ['required', new Enum(TaskPriority::class)],
            'description' => 'required|string|max:2048|min:1',
            'photo' => ['sometimes', File::image()->max(10 * 1024)],
        ]);
        $this->taskService->createOrUpdateTask(
            new TaskForm(
                $id,
                $request->categoryId,
                TaskPriority::from($request->priority),
                $request->description,
                $request->file('photo')
            )
        );
        return redirect()->route('tasks.index');
    }

    public function toggleFinished($id): void
    {
        $this->taskService->toggleTaskFinished($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->taskService->deleteTask($id);
        return redirect()->back();
    }

    public function deleteFinished()
    {
        $this->taskService->deleteFinishedTasks();
        return redirect()->back();
    }
}
