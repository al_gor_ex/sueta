<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Notification\NotificationFilters;
use App\Service\Notification\INotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct(
        private INotificationService $notificationService
    )
    {
    }

    public function index(Request $request)
    {
        $result = $this->notificationService->getNotificationsList(
            new NotificationFilters(
                $request->query('currentPage', 1)
            )
        );
        return view('dashboard.notifications', compact('result'));
    }

    public function confirm(int $id)
    {
        $this->notificationService->confirmNotification($id);
        return redirect()->back();
    }

    public function clear()
    {
        $this->notificationService->clearNotifications();
        return redirect()->back();
    }
}
