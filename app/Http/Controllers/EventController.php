<?php

namespace App\Http\Controllers;

use App\Http\Dto\Requests\Event\EventFilters;
use App\Http\Dto\Requests\Event\EventForm;
use App\Service\Event\IEventService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EventController extends Controller
{
    public function __construct(
        private IEventService $eventService
    )
    {
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        if ($request->hasCookie('eventFilters_dateFrom') &&
            $request->hasCookie('eventFilters_dateTo')) {
            $filters = new EventFilters(
                Carbon::createFromIsoFormat('Y-MM-DD', $request->cookie('eventFilters_dateFrom'))->startOfDay(),
                Carbon::createFromIsoFormat('Y-MM-DD', $request->cookie('eventFilters_dateTo'))->endOfDay()
            );
        } else {
            $filters = null;
        }
        $result = $this->eventService->getEventsList($filters);
        return view('registry.events.events-list', compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('registry.events.new-edit-event', ['id' => -1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required|string|max:255|min:1',
            'startsAt' => 'required|date|after:now'
        ]);
        $this->eventService->createOrUpdateEvent(
            new EventForm(null, $request->description, $request->startsAt)
        );
        return redirect()->route('events.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $event = $this->eventService->getEventEditor($id);
        return view('registry.events.new-edit-event', compact('id', 'event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required|string|max:255|min:1',
            'startsAt' => 'required|date|after:now'
        ]);
        $this->eventService->createOrUpdateEvent(
            new EventForm($id, $request->description, $request->startsAt)
        );
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $this->eventService->deleteEvent($id);
        return redirect()->back();
    }

    public function deletePast()
    {
        $this->eventService->deletePastEvents();
        return redirect()->back();
    }
}
