function restoreFilters(): void
{
    let url = new URL(location.href)
    if (url.searchParams.has('monthsDeltaFromNow') === false) {
        url.searchParams.set('monthsDeltaFromNow', String(0))
        location.href = url.href
    }
}

function changeMonthsDelta(diff: number, direction: 'forward' | 'backward'): void
{
    let url = new URL(location.href)
    let delta = Number(url.searchParams.get('monthsDeltaFromNow'))
    switch (direction) {
        case "backward":
            delta -= diff
            break
        case "forward":
            delta += diff
            break
    }
    url.searchParams.set('monthsDeltaFromNow', String(delta))
    location.href = url.href
}


restoreFilters()
let buttonPrevMonth = document.getElementById('btn-prev-month') as HTMLButtonElement
buttonPrevMonth.onclick = () => changeMonthsDelta(1, 'backward')
let buttonNextMonth = document.getElementById('btn-next-month') as HTMLButtonElement
buttonNextMonth.onclick = () => changeMonthsDelta(1, 'forward')
