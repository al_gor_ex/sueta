import Cookies from "../../../node_modules/js-cookie/dist/js.cookie.mjs";

let finishedRadioButtons = document.querySelectorAll<HTMLInputElement>('input[name=onlyFinished]')
let categoryRadioButtons = document.querySelectorAll<HTMLInputElement>('input[name=categoryId]')

function restoreFilters(): void {
    restoreFinishedFilter()
    restoreCategoryIdFilter()
}

function restoreFinishedFilter(): void {
    let valueFromCookie = Cookies.get('taskFilters.onlyFinished')
    if (valueFromCookie === undefined) {
        finishedRadioButtons[0].checked = true
        Cookies.set('taskFilters.onlyFinished', finishedRadioButtons[0].value)
    } else {
        for (let radioBtn of finishedRadioButtons) {
            if (radioBtn.value === valueFromCookie) {
                radioBtn.checked = true
                break
            }
        }
    }
}

function restoreCategoryIdFilter(): void {
    let valueFromCookie = Cookies.get('taskFilters.categoryId')
    if (valueFromCookie === undefined) {
        categoryRadioButtons[0].checked = true
        Cookies.set('taskFilters.categoryId', categoryRadioButtons[0].value)
    } else {
        for (let radioBtn of categoryRadioButtons) {
            if (radioBtn.value === valueFromCookie) {
                radioBtn.checked = true
                break
            }
        }
    }
}

function onFinishedSelect(newValue: string): void {
    Cookies.set('taskFilters.onlyFinished', newValue)
    location.reload()
}

function onCategoryIdSelect(newValue: string): void {
    Cookies.set('taskFilters.categoryId', newValue)
    location.reload()
}

function onBtnShowClick(button: HTMLButtonElement): void {
    let card = button.parentElement
    let textToShow = card.querySelector<HTMLSpanElement>('.collapsible-text')
    button.classList.add('display-none')
    textToShow.classList.remove('display-none')
}



function onDeleteTaskButtonClick(button: HTMLButtonElement): void {
    if (confirm('Задача будет удалена')) {
        button.closest('form').submit()
    }
}

function onDeleteFinishedTasksButtonClick(button: HTMLButtonElement): void {
    if (confirm('Выполненные задачи будут удалены из всех категорий')) {
        button.closest('form').submit()
    }
}

restoreFilters()

let showFullDescriptionButtons = document.getElementsByClassName('show-button')
for (let showBtn of showFullDescriptionButtons) {
    (showBtn as HTMLButtonElement).onclick = () => onBtnShowClick(showBtn as HTMLButtonElement)
}

let deleteTaskButtons = document.querySelectorAll<HTMLButtonElement>('button[id^=btn-delete-task-]')
for (let button of deleteTaskButtons) {
    button.onclick = (event) => onDeleteTaskButtonClick(event.target as HTMLButtonElement)
}

(document.getElementById('btn-delete-finished-tasks') as HTMLButtonElement).onclick = (event) => {
    onDeleteFinishedTasksButtonClick(event.target as HTMLButtonElement)
}

for (let radioBtn of finishedRadioButtons) {
    radioBtn.onchange = (event) => {
        onFinishedSelect((event.target as HTMLInputElement).value)
    }
}
for (let radioBtn of categoryRadioButtons) {
    radioBtn.onchange = (event) => {
        onCategoryIdSelect((event.target as HTMLInputElement).value)
    }
}

