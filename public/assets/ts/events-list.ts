import Cookies from "../../../node_modules/js-cookie/dist/js.cookie.mjs";

function onDeleteEventButtonClick(event: MouseEvent) {
    if (confirm('Мероприятие будет удалено')) {
        let parentForm = (event.target as HTMLButtonElement).closest('form')
        parentForm.submit()
    }
}

function onDeletePastEventsButtonClick(event: MouseEvent) {
    if (confirm('Все мероприятия, начинающиеся раньше текущего времени, будут удалены')) {
        let parentForm = (event.target as HTMLButtonElement).closest('form')
        parentForm.submit()
    }
}

function restoreFilters(): void {
    if (Cookies.get('eventFilters.dateFrom') !== undefined &&
        Cookies.get('eventFilters.dateFrom') !== undefined) {
        fillFiltersFormWithCookies()
    } else {
        (document.forms['dateFiltersForm'] as HTMLFormElement).reset()
        hideResetFiltersButton()
    }
}

function fillFiltersFormWithCookies(): void {
    let form = document.forms['dateFiltersForm'] as HTMLFormElement
    (form['dateFrom'] as HTMLInputElement).value = Cookies.get('eventFilters.dateFrom');
    (form['dateTo'] as HTMLInputElement).value = Cookies.get('eventFilters.dateTo')
}

function hideResetFiltersButton(): void {
    let resetBtn = document.getElementById('btn-reset-filters')
    resetBtn.classList.add('display-none')
}

function applyFilters(): void {
    if (validateFiltersForm() === false) {
        alert('Правая дата фильтра должна быть больше левой')
        return
    }
    let form = document.forms['dateFiltersForm'] as HTMLFormElement
    let dateFrom = (form['dateFrom'] as HTMLInputElement).value
    let dateTo = (form['dateTo'] as HTMLInputElement).value
    Cookies.set('eventFilters.dateFrom', dateFrom)
    Cookies.set('eventFilters.dateTo', dateTo)
    location.reload()
}

function validateFiltersForm(): boolean {
    let form = document.forms['dateFiltersForm'] as HTMLFormElement
    let dateFrom = (form['dateFrom'] as HTMLInputElement).valueAsDate
    let dateTo = (form['dateTo'] as HTMLInputElement).valueAsDate
    return (dateFrom < dateTo)
}

function resetFilters(): void {
    Cookies.remove('eventFilters.dateFrom')
    Cookies.remove('eventFilters.dateTo')
    location.reload()
}

restoreFilters()

let deleteEventButtons = document.querySelectorAll<HTMLButtonElement>('button[id^=btn-delete-event-]')
for (let button of deleteEventButtons) {
    button.onclick = onDeleteEventButtonClick
}

let deletePastEventsButton = document.getElementById('btn-delete-past-events') as HTMLButtonElement
deletePastEventsButton.onclick = onDeletePastEventsButtonClick

let buttonApplyFilters = document.getElementById('btn-apply-filters') as HTMLButtonElement
buttonApplyFilters.onclick = applyFilters

let buttonResetFilters = document.getElementById('btn-reset-filters') as HTMLButtonElement
buttonResetFilters.onclick = resetFilters
