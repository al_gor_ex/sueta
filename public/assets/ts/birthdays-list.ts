import Cookies from "../../../node_modules/js-cookie/dist/js.cookie.mjs";

function onDeleteBirthdayButtonClick(button: HTMLButtonElement): void {
    if (confirm('День рождения будет удалён')) {
        button.closest('form').submit()
    }
}

function restoreFilters(): void {
    let sortingSelect = document.getElementById('sorting-select') as HTMLSelectElement
    if (Cookies.get('birthdayFilters.sortBy') === undefined) {
        sortingSelect.selectedIndex = 0
        Cookies.set('birthdayFilters.sortBy', sortingSelect.options.item(0).value)
    } else {
        let sortingFromCookie = Cookies.get('birthdayFilters.sortBy')
        for (let option of sortingSelect.options) {
            if (option.value === sortingFromCookie) {
                option.selected = true
                break
            }
        }
    }
}

function onSortingSelect(newValue: string): void {
    Cookies.set('birthdayFilters.sortBy', newValue)
    location.reload()
}

restoreFilters()

let deleteBirthdayButtons = document.querySelectorAll<HTMLButtonElement>('button[id^=btn-delete-birthday-]')
for (let button of deleteBirthdayButtons) {
    button.onclick = (event) => onDeleteBirthdayButtonClick(event.target as HTMLButtonElement)
}
(document.getElementById('sorting-select') as HTMLSelectElement).onchange = (event) => {
    onSortingSelect((event.target as HTMLSelectElement).value)
}
