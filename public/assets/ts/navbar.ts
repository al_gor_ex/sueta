import Cookies from "../../../node_modules/js-cookie/dist/js.cookie.mjs";

let btnLogout = document.getElementById('btn-logout') as HTMLDivElement
if (btnLogout !== null) {
    btnLogout.onclick = () => {
        Cookies.remove('birthdayFilters.sortBy')
        Cookies.remove('eventFilters.dateFrom')
        Cookies.remove('eventFilters.dateTo')
        Cookies.remove('taskFilters.onlyFinished')
        Cookies.remove('taskFilters.categoryId')
        btnLogout.closest('form').submit()
    }
}
