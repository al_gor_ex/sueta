function onDeleteRoutineButtonClick(button: HTMLButtonElement): void {
    if (confirm('Рутина будет удалена')) {
        button.closest('form').submit()
    }
}

let deleteRoutineButtons = document.querySelectorAll<HTMLButtonElement>('button[id^=btn-delete-routine-]')
for (let button of deleteRoutineButtons) {
    button.onclick = (event) => onDeleteRoutineButtonClick(event.target as HTMLButtonElement)
}
