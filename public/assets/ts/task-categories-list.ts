function onDeleteCategoryButtonClick(button: HTMLButtonElement): void {
    if (confirm('Данная категория и все задачи, относящиеся к ней, будут удалены')) {
        button.closest('form').submit()
    }
}

let deleteCategoryButtons = document.querySelectorAll<HTMLButtonElement>('button[id^=btn-delete-category-]')
for (let button of deleteCategoryButtons) {
    button.onclick = (event) => onDeleteCategoryButtonClick(event.target as HTMLButtonElement)
}
