function restoreQueryParams(): void {
    let url = new URL(location.href)
    if (url.searchParams.has('currentPage') === false) {
        url.searchParams.set('currentPage', String(1))
        location.href = url.href
    }
}

function flipPage(direction: 'prev' | 'next', count: number = 1): void {
    if (direction === 'prev') {
        count *= -1
    }
    let url = new URL(location.href)
    let curPage = Number(url.searchParams.get('currentPage'))
    url.searchParams.set('currentPage', String(curPage + count))
    location.href = url.href
}

function toggleCollapseMark(header: HTMLDivElement): void {
    let mark = header.querySelector('span.day-collapse-mark') as HTMLSpanElement
    let markText = mark.innerText
    markText = (markText === '-') ? '+' : '-'
    mark.innerHTML = markText
}

function toggleDayContainer(id: number): void {
    let container = document.getElementById(`day-container-${id}`) as HTMLDivElement;
    container.classList.toggle('display-none')
}

function onBtnClearNotificationsClick(button: HTMLButtonElement): void {
    if (confirm('Все уведомления будут удалены')) {
        button.closest('form').submit()
    }
}

function onBtnConfirmNotificationClick(button: HTMLButtonElement): void {
    if (confirm('Уведомление будет прочитано')) {
        button.closest('form').submit()
    }
}

restoreQueryParams();
let btnPrevPage = document.getElementById('btn-prev-page') as HTMLDivElement;
if (btnPrevPage !== undefined) {
    btnPrevPage.onclick = () => {
        flipPage('prev')
    }
}
let btnNextPage = document.getElementById('btn-next-page') as HTMLDivElement;
if (btnNextPage !== undefined) {
    btnNextPage.onclick = () => {
        flipPage('next')
    }
}
let dayHeaders = document.querySelectorAll<HTMLDivElement>('div[id^=day-header-]')
for (let header of dayHeaders) {
    let id = Number(header.id.replace('day-header-', ''))
    header.onclick = () =>  {
        toggleCollapseMark(header)
        toggleDayContainer(id)
    }
}
let btnClearNotifications = document.getElementById('btn-clear-notifications') as HTMLButtonElement
btnClearNotifications.onclick = () => onBtnClearNotificationsClick(btnClearNotifications)

let confirmNotificationButtons = document.querySelectorAll<HTMLButtonElement>('[id^=confirm-notification-]')
for (let button of confirmNotificationButtons) {
    button.onclick = () => onBtnConfirmNotificationClick(button)
}
