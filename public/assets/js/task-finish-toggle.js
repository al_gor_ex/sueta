function onBtnToggleTaskFinishedClick(button) {
    let taskId = Number(button.id.replace('btn-toggle-task-finished-', ''))
    axios.put(
        `http://localhost/sueta/public/index.php/tasks/${taskId}/toggle-finished`,
        {},
        {
            headers: {
                Accept: 'application/json'
            },
            withCredentials: true
        }
    ).then(() => {
        toggleTaskFinishedIcon(taskId)
        alert('Статус задачи изменён')
    }).catch((error) => {
        console.log(error)
        alert('Не удалось изменить статус задачи (см. консоль)')
    })
}

function toggleTaskFinishedIcon(taskId) {
    let button = document.getElementById(`btn-toggle-task-finished-${taskId}`)
    let checkmarkIcon = button.querySelector('img')
    let iconClassList = checkmarkIcon.classList;
    if (iconClassList.contains('hidden')) {
        iconClassList.remove('hidden')
    } else {
        iconClassList.add('hidden')
    }
}

let toggleTaskFinishedButtons = document.querySelectorAll('div[id^=btn-toggle-task-finished-]')
for (let button of toggleTaskFinishedButtons) {
    button.onclick = (event) => onBtnToggleTaskFinishedClick(event.target)
}
