<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Категория</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-edit-task-category.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/cancel-button.js')}}"></script>
</head>
@php
    $isCreating = ($id == -1);
    $isUpdating = !$isCreating;
@endphp
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post"
              @if($isCreating)
                  action="{{route('categories.store')}}"
              @else
                  action="{{route('categories.update', ['category' => $id])}}"
              @endif
              autocomplete="off" class="category-form">
            @csrf
            @if ($isUpdating)
                @method('PUT')
            @endif
            @include('components.form-errors')
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Название
                    </div>
                    <div>
                        <input name="name" type="text"
                               @if ($isCreating || $errors->any())
                                   value="{{old('name')}}"
                               @else
                                   value="{{$category->name}}"
                               @endif
                               class="dark-text-input input-width" required>
                    </div>
                    <div class="input-label">
                        Цвет
                    </div>
                    <div class="color-input-flex">
                        <input name="backgroundColor" type="color"
                               @if($errors->any())
                                   value="{{old('backgroundColor')}}"
                               @elseif($isCreating)
                                   value="{{'#' . base_convert(random_int(0, 16_777_215), 10, 16)}}"
                               @else
                                   value="{{$category->backgroundColor}}"
                               @endif
                               required>
                    </div>
                    <div class="buttons-flex">
                        <button type="submit" class="dark-button">
                            Сохранить
                        </button>
                        <button id="btn-cancel" type="button" class="dark-cancel-button">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
