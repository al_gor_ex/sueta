<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Список задач</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/tasks-list.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script type="module" src="{{asset('assets/compiled-js/tasks-list.js')}}"></script>
    <script defer src="{{asset('assets/js/lib/axios.min.js')}}"></script>
    <script defer src="{{asset('assets/js/task-finish-toggle.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="flex-between">
            <form method="get" action="{{route('categories.index')}}">
                <button type="submit" class="dark-button">
                    Список категорий
                </button>
            </form>
            <div @class(['flex-right', 'hidden' => empty($categoriesList->categories)])>
                <form method="get" action="{{route('tasks.create')}}" class="inline">
                    <button type="submit" class="dark-create-button button-icon-flex mr">
                        <img src="{{asset('assets/img/icons/plus.png')}}">
                        <span>
                            Новая задача
                        </span>
                    </button>
                </form>
                <form method="post" action="{{route('tasks.delete-finished')}}">
                    @method('DELETE')
                    @csrf
                    <button id="btn-delete-finished-tasks" type="button"
                            class="dark-cancel-button button-icon-flex">
                        <img src="{{asset('assets/img/icons/trash.png')}}">
                        <span>
                            Удалить выполненные
                        </span>
                    </button>
                </form>
            </div>
        </div>
        <form name="filters" @class(['flex-center', 'hidden' => empty($categoriesList->categories)])>
            <div class="input-label mr">
                Отображать только
            </div>
            <div class="radios-list-wrapper mr">
                <div class="radio-option">
                    <input id="notFinished" type="radio" name="onlyFinished" value="0" class="dark-checkbox">
                    <label for="notFinished" class="radio-option-label">
                        Невыполненные
                    </label>
                </div>
                <div class="radio-option">
                    <input id="finished" type="radio" name="onlyFinished" value="1" class="dark-checkbox">
                    <label for="finished" class="radio-option-label">
                        Выполненные
                    </label>
                </div>
            </div>
            <div class="input-label mr">
                из категории
            </div>
            <div class="radios-list-wrapper">
                <div class="radio-option">
                    <input id="categoryId-null" type="radio" name="categoryId" value="null" class="dark-checkbox">
                    <label for="categoryId-null" class="radio-option-label">
                        Любая
                    </label>
                </div>
                @foreach($categoriesList->categories as $category)
                    <div class="radio-option">
                        <input id="categoryId{{$category->id}}" type="radio" name="categoryId"
                               value="{{$category->id}}" class="dark-checkbox">
                        <label for="categoryId{{$category->id}}" class="radio-option-label"
                               style="color: {{$category->fontColor}}; background-color: {{$category->backgroundColor}}">
                            {{$category->name}}
                        </label>
                    </div>
                @endforeach
            </div>
        </form>
        @if (empty($catalog->columns))
            <div class="empty-message-flex">
                <div class="empty-message">
                    Задачи отсутствуют
                </div>
            </div>
        @else
            <div class="flex-center">
                <div class="tasks-catalog">
                    @foreach($catalog->columns as $column)
                        <div class="column">
                            @foreach($column as $task)
                                <div class="task-card">
                                    <div class="card-controls-panel">
                                        <div id="btn-toggle-task-finished-{{$task->id}}" class="card-checkbox-wrapper">
                                            <img src="{{asset('assets/img/icons/checkmark.png')}}"
                                                @class(['checkmark-icon', 'hidden' => $task->isFinished == false])>
                                        </div>
                                        <div class="category-label"
                                             style="color: {{$task->categoryFontColor}}; background-color: {{$task->categoryBackgroundColor}}">
                                            {{$task->categoryName}}
                                        </div>
                                        @switch($task->priority)
                                            @case(\App\Models\Enums\TaskPriority::HIGH)
                                                <img src="{{asset('assets/img/icons/priority-high.png')}}">
                                                @break
                                            @case(\App\Models\Enums\TaskPriority::MEDIUM)
                                                <img src="{{asset('assets/img/icons/priority-medium.png')}}">
                                                @break
                                            @case(\App\Models\Enums\TaskPriority::LOW)
                                                <img src="{{asset('assets/img/icons/priority-low.png')}}">
                                                @break
                                        @endswitch
                                        <div>
                                            <form method="get" action="{{route('tasks.edit', ['task' => $task->id])}}"
                                                class="inline">
                                                <button type="submit" class="dark-button tight-button">
                                                    <img src="{{asset('assets/img/icons/edit.png')}}">
                                                </button>
                                            </form>
                                            <form method="post" action="{{route('tasks.destroy', ['task' => $task->id])}}"
                                                class="inline">
                                                @method('DELETE')
                                                @csrf
                                                <button id="btn-delete-task-{{$task->id}}"
                                                        type="button" class="dark-button tight-button ml">
                                                    <img src="{{asset('assets/img/icons/delete.png')}}">
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card-separator"></div>
                                    <div class="card-content">
                                        @isset($task->photoPreviewUrl)
                                            <a href="{{$task->photoFullSizeUrl}}" target="_blank">
                                                <img src="{{$task->photoPreviewUrl}}" class="card-image">
                                            </a>
                                        @endisset
                                        {{$task->visibleDescription}}
                                        @isset($task->hiddenDescription)
                                            <button type="button" class="dark-button show-button">
                                                ...
                                            </button>
                                            <span class="collapsible-text display-none">
                                                {{$task->hiddenDescription}}
                                            </span>
                                        @endisset
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
</body>
</html>
