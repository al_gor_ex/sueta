<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Список категорий</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/list-table.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/task-categories-list.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/task-categories-list.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="flex-between">
            <form method="get" action="{{route('tasks.index')}}" class="inline">
                <button type="submit" class="dark-button button-icon-flex">
                    <img src="{{asset('assets/img/icons/arrow-left.png')}}">
                    <span>
                        Список задач
                    </span>
                </button>
            </form>
            <form method="get" action="{{route('categories.create')}}">
                <button type="submit" class="dark-create-button button-icon-flex">
                    <img src="{{asset('assets/img/icons/plus.png')}}">
                    <span>
                    Новая категория
                </span>
                </button>
            </form>
        </div>
        @if(count($result->categories) == 0)
            <div class="empty-message-flex">
                <div class="empty-message">
                    Категории отсутствуют
                </div>
            </div>
        @else
            <div class="flex-center">
                <table class="list-table">
                    <thead>
                    <tr>
                        <td>
                            Название
                        </td>
                        <td>
                            Невыполненных задач
                        </td>
                        <td>
                            По приоритетам
                        </td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result->categories as $category)
                        <tr>
                            <td style="color: {{$category->fontColor}}; background-color: {{$category->backgroundColor}}">
                                {{$category->name}}
                            </td>
                            <td class="centered-cell">
                                {{$category->tasksCount}}
                            </td>
                            <td class="centered-cell">
                                <span class="priority-group-wrapper">
                                    <img src="{{asset('assets/img/icons/priority-high.png')}}">
                                    <span class="priority-count">
                                        {{$category->tasksCountByPriority[\App\Models\Enums\TaskPriority::HIGH->value]}}
                                    </span>
                                </span>
                                <span class="priority-group-wrapper">
                                    <img src="{{asset('assets/img/icons/priority-medium.png')}}">
                                    <span class="priority-count">
                                        {{$category->tasksCountByPriority[\App\Models\Enums\TaskPriority::MEDIUM->value]}}
                                    </span>
                                </span>
                                <span class="priority-group-wrapper">
                                    <img src="{{asset('assets/img/icons/priority-low.png')}}">
                                    <span class="priority-count">
                                        {{$category->tasksCountByPriority[\App\Models\Enums\TaskPriority::LOW->value]}}
                                    </span>
                                </span>
                            </td>
                            <td>
                                <form method="get" action="{{route('categories.edit', ['category' => $category->id])}}"
                                      class="inline">
                                    <button type="submit" class="dark-button tight-button">
                                        <img src="{{asset('assets/img/icons/edit.png')}}">
                                    </button>
                                </form>
                                <form method="post"
                                      action="{{route('categories.destroy', ['category' => $category->id])}}"
                                      class="inline">
                                    @method('DELETE')
                                    @csrf
                                    <button id="btn-delete-category-{{$category->id}}" type="button"
                                            class="dark-button tight-button ml">
                                        <img src="{{asset('assets/img/icons/delete.png')}}">
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
