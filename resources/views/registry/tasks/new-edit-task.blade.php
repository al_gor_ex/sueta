<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Задача</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-edit-task.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/cancel-button.js')}}"></script>
</head>
@php
    $isCreating = ($id == -1);
    $isUpdating = !$isCreating;
@endphp
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post"
              @if($isCreating)
                  action="{{route('tasks.store')}}"
              @else
                  action="{{route('tasks.update', ['task' => $id])}}"
              @endif
              enctype="multipart/form-data" autocomplete="off" class="task-form">
            @if ($isUpdating)
                @method('PUT')
            @endif
            @csrf
            @include('components.form-errors')
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Категория
                    </div>
                    <div class="radios-list-wrapper">
                        @foreach($categoriesList->categories as $category)
                            <div class="radio-option">
                                <input id="categoryId{{$category->id}}" type="radio" name="categoryId"
                                       value="{{$category->id}}" class="dark-checkbox"
                                        @if($errors->any())
                                            @checked($category->id == old('categoryId'))
                                        @elseif($isCreating)
                                            @checked($loop->first)
                                        @elseif($isUpdating)
                                            @checked($task->categoryId == $category->id)
                                        @endif>
                                <label for="categoryId{{$category->id}}" class="radio-option-label"
                                       style="color: {{$category->fontColor}}; background-color: {{$category->backgroundColor}}">
                                    {{$category->name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="input-label">
                        Приоритет
                    </div>
                    <div class="radios-list-wrapper">
                        <div class="radio-option">
                            <input id="priority3" type="radio" name="priority"
                                   value="{{\App\Models\Enums\TaskPriority::HIGH->value}}" class="dark-checkbox"
                                    @if ($errors->any())
                                        @checked(old('priority') == \App\Models\Enums\TaskPriority::HIGH->value)
                                    @elseif($isUpdating)
                                        @checked($task->priority == \App\Models\Enums\TaskPriority::HIGH)
                                    @endif>
                            <label for="priority3" class="radio-option-label">
                                <img src="{{asset('assets/img/icons/priority-high.png')}}" class="option-icon">
                                <span>
                                    Высокий
                                </span>
                            </label>
                        </div>
                        <div class="radio-option">
                            <input id="priority2" type="radio" name="priority"
                                   value="{{\App\Models\Enums\TaskPriority::MEDIUM->value}}" class="dark-checkbox"
                                    @if ($errors->any())
                                        @checked(old('priority') == \App\Models\Enums\TaskPriority::MEDIUM->value)
                                    @elseif($isCreating)
                                        checked
                                    @elseif($isUpdating)
                                        @checked($task->priority == \App\Models\Enums\TaskPriority::MEDIUM)
                                    @endif>
                            <label for="priority2" class="radio-option-label">
                                <img src="{{asset('assets/img/icons/priority-medium.png')}}">
                                <span>
                                    Средний
                                </span>
                            </label>
                        </div>
                        <div class="radio-option">
                            <input id="priority1" type="radio" name="priority"
                                   value="{{\App\Models\Enums\TaskPriority::LOW->value}}" class="dark-checkbox"
                                    @if ($errors->any())
                                        @checked(old('priority') == \App\Models\Enums\TaskPriority::LOW->value)
                                    @elseif($isUpdating)
                                        @checked($task->priority == \App\Models\Enums\TaskPriority::LOW)
                                    @endif>
                            <label for="priority1" class="radio-option-label">
                                <img src="{{asset('assets/img/icons/priority-low.png')}}">
                                <span>
                                    Низкий
                                </span>
                            </label>
                        </div>
                    </div>
                    <div class="input-label">
                        Описание
                    </div>
                    <div>
                        @if ($isCreating || $errors->any())
                            <textarea name="description" class="dark-text-area" spellcheck="false">{{old('description')}}</textarea>
                        @else
                            <textarea name="description" class="dark-text-area" spellcheck="false">{{$task->description}}</textarea>
                        @endif
                    </div>
                    <div class="input-label">
                        Фото (необязательно)
                    </div>
                    @if($isUpdating)
                        <div class="field-description">
                            (чтобы оставить существующее фото, <br> оставьте поле пустым)
                        </div>
                    @endif
                    <div class="file-input-flex">
                        <input name="photo" type="file" accept=".jpg,.jpeg,.png">
                    </div>
                    <div class="buttons-flex">
                        <button type="submit" class="dark-button">
                            Сохранить
                        </button>
                        <button id="btn-cancel" type="button" class="dark-cancel-button">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
