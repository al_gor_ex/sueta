<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Список мероприятий</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/list-table.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/events-list.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script type="module" src="{{asset('assets/compiled-js/events-list.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="flex-right">
            <form action="{{route('events.create')}}">
                <button type="submit" class="dark-create-button button-icon-flex mr">
                    <img src="{{asset('assets/img/icons/plus.png')}}">
                    <span>
                        Новое мероприятие
                    </span>
                </button>
            </form>
            <form method="post" action="{{route('events.delete-past')}}">
                @method('DELETE')
                @csrf
                <button id="btn-delete-past-events" type="button"
                        class="dark-cancel-button button-icon-flex">
                    <img src="{{asset('assets/img/icons/trash.png')}}">
                    <span>
                    Удалить прошедшие
                </span>
                </button>
            </form>
        </div>
        <div>
            <form name="dateFiltersForm" class="flex-center">
                <span class="input-label mr">
                    с
                </span>
                <input type="date" name="dateFrom" required class="mr dark-text-input dark-datetime-input">
                <span class="input-label mr">
                    по
                </span>
                <input type="date" name="dateTo" required class="mr dark-text-input dark-datetime-input">
                <button id="btn-apply-filters" type="button" class="dark-button mr">
                    Фильтровать
                </button>
                <button id="btn-reset-filters" type="button" class="dark-cancel-button">
                    Сбросить
                </button>
            </form>
        </div>
        @if (count($result->events) == 0)
            <div class="empty-message-flex">
                <div class="empty-message">
                    Мероприятия отсутствуют
                </div>
            </div>
        @else
            <div class="flex-center">
                <table class="list-table">
                    <thead>
                    <tr>
                        <td>
                            Описание
                        </td>
                        <td>
                            Начало
                        </td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result->events as $event)
                        <tr>
                            <td>
                                {{$event->description}}
                            </td>
                            <td>
                                {{$event->startsAt}}
                            </td>
                            <td class="buttons-cell">
                                <form method="get" action="{{route('events.edit', ['event' => $event->id])}}"
                                    class="inline">
                                    <button type="submit" class="dark-button tight-button">
                                        <img src="{{asset('assets/img/icons/edit.png')}}">
                                    </button>
                                </form>
                                <form method="post" action="{{route('events.destroy', ['event' => $event->id])}}"
                                    class="inline">
                                    @method('DELETE')
                                    @csrf
                                    <button id="btn-delete-event-{{$event->id}}" type="button"
                                            class="dark-button tight-button ml">
                                        <img src="{{asset('assets/img/icons/delete.png')}}">
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
