<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Мероприятие</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-edit-event.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/cancel-button.js')}}"></script>
</head>
@php
    $isCreating = ($id == -1);
    $isUpdating = !$isCreating;
@endphp
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post" autocomplete="off"
              @if ($isCreating)
                  action="{{route('events.store')}}"
              @else
                  action="{{route('events.update', ['event' => $id])}}"
              @endif
              class="event-form">
            @if ($isUpdating)
                @method('PUT')
            @endif
            @include('components.form-errors')
            @csrf
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Описание мероприятия
                    </div>
                    <div>
                        <input name="description" type="text"
                               @if ($isCreating || $errors->any())
                                   value="{{old('description')}}"
                               @else
                                   value="{{$event->description}}"
                               @endif
                               class="dark-text-input input-width" required>
                    </div>
                    <div class="input-label">
                        Дата и время начала
                    </div>
                    <div>
                        <input name="startsAt" type="datetime-local"
                               @if ($isCreating || $errors->any())
                                   value="{{old('startsAt')}}"
                               @else
                                   value="{{$event->startsAt}}"
                               @endif
                               class="dark-text-input dark-datetime-input input-width" required>
                    </div>
                    <div class="buttons-flex">
                        <button type="submit" class="dark-button">
                            Сохранить
                        </button>
                        <button id="btn-cancel" type="button" class="dark-cancel-button">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
