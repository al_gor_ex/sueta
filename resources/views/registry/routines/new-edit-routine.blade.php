<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Рутина</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-edit-routine.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/cancel-button.js')}}"></script>
</head>
@php
    $isCreating = ($id == -1);
    $isUpdating = !$isCreating;
@endphp
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post" autocomplete="off"
              @if($isCreating)
                  action="{{route('routines.store')}}"
              @else
                  action="{{route('routines.update', ['routine' => $id])}}"
              @endif
              class="routine-form">
            @csrf
            @if ($isUpdating)
                @method('PUT')
            @endif
            @include('components.form-errors')
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Описание рутины
                    </div>
                    <div>
                        <input name="description" type="text"
                               @if ($isCreating || $errors->any())
                                   value="{{old('description')}}"
                               @else
                                   value="{{$routine->description}}"
                               @endif
                               class="dark-text-input input-width" required>
                    </div>
                    <div class="input-label">
                        Дата начала отсчёта
                    </div>
                    <div>
                        <input name="countStartDate" type="date"
                               @if($isCreating || $errors->any())
                                   value="{{old('countStartDate')}}"
                               @else
                                   value="{{$routine->countStartDate}}"
                               @endif
                               class="dark-text-input dark-datetime-input input-width" required>
                    </div>
                    <div class="input-label">
                        Периодичность в днях
                    </div>
                    <div>
                        <input name="periodLengthDays" type="number"
                               @if($isCreating || $errors->any())
                                   value="{{old('periodLengthDays')}}"
                               @else
                                   value="{{$routine->periodLengthDays}}"
                               @endif
                               class="dark-text-input" min="1" required>
                    </div>
                    <div class="buttons-flex">
                        <button type="submit" class="dark-button">
                            Сохранить
                        </button>
                        <button id="btn-cancel" type="button" class="dark-cancel-button">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
