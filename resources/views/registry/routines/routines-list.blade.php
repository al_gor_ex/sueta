<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Список рутины</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/list-table.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/routines-list.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/routines-list.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="flex-right">
            <form method="get" action="{{route('routines.create')}}">
                <button type="submit" class="dark-create-button button-icon-flex">
                    <img src="{{asset('assets/img/icons/plus.png')}}">
                    <span>
                        Новая рутина
                    </span>
                </button>
            </form>
        </div>
        @if(count($result->routines) == 0)
            <div class="empty-message-flex">
                <div class="empty-message">
                    Рутина отсутствует
                </div>
            </div>
        @else
            <div class="flex-center">
                <table class="list-table">
                    <thead>
                    <tr>
                        <td>
                            Описание
                        </td>
                        <td>
                            Отсчёт с
                        </td>
                        <td>
                            Периодичность в днях
                        </td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result->routines as $routine)
                        <tr>
                            <td>
                                {{$routine->description}}
                            </td>
                            <td>
                                {{$routine->countStartDate}}
                            </td>
                            <td class="centered-cell">
                                {{$routine->periodLengthDays}}
                            </td>
                            <td class="buttons-cell">
                                <form method="get" action="{{route('routines.edit', ['routine' => $routine->id])}}"
                                    class="inline">
                                    <button type="submit" class="dark-button tight-button">
                                        <img src="{{asset('assets/img/icons/edit.png')}}">
                                    </button>
                                </form>
                                <form method="post" action="{{route('routines.destroy', ['routine' => $routine->id])}}"
                                    class="inline">
                                    @method('DELETE')
                                    @csrf
                                    <button id="btn-delete-routine-{{$routine->id}}" type="button"
                                            class="dark-button tight-button ml">
                                        <img src="{{asset('assets/img/icons/delete.png')}}">
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
