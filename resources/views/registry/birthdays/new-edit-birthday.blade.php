<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - День рождения</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/new-edit-birthday.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/cancel-button.js')}}"></script>
</head>
@php
    $isCreating = ($id == -1);
    $isUpdating = !$isCreating;
@endphp
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post"
              @if ($isCreating)
                  action="{{route('birthdays.store')}}"
              @else
                  action="{{route('birthdays.update', ['birthday' => $id])}}"
              @endif
              enctype="multipart/form-data"
              autocomplete="off" class="birthday-form">
            @if ($isUpdating)
                @method('PUT')
            @endif
            @include('components.form-errors')
            @csrf
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Имя человека
                    </div>
                    <div>
                        <input name="personName" type="text"
                               @if ($isCreating || $errors->any())
                                   value="{{old('personName')}}"
                               @else
                                   value="{{$birthday->personName}}"
                               @endif
                               class="dark-text-input input-width" required>
                    </div>
                    <div class="input-label">
                        Фото (необязательно)
                    </div>
                    @if($isUpdating)
                        <div class="field-description">
                            (чтобы оставить существующее фото, <br> оставьте поле пустым)
                        </div>
                    @endif
                    <div class="file-input-flex">
                        <input name="personPhoto" type="file" accept=".jpg,.jpeg,.png">
                    </div>
                    <div class="input-label">
                        Дата рождения
                    </div>
                    <div class="field-description">
                        (если год неизвестен, укажите любой и поставьте галочку)
                    </div>
                    <div>
                        <input name="isYearUnknown" type="checkbox"
                               @if ($isCreating || $errors->any())
                                   @checked(old('isYearUnknown'))
                               @else
                                   @checked($birthday->isYearUnknown)
                               @endif
                               class="dark-checkbox">
                        <input name="date" type="date"
                               @if ($isCreating || $errors->any())
                                   value="{{old('date')}}"
                               @else
                                   value="{{$birthday->date}}"
                               @endif
                               class="dark-text-input dark-datetime-input input-width" required>
                    </div>
                    <div class="buttons-flex">
                        <button type="submit" class="dark-button">
                            Сохранить
                        </button>
                        <button id="btn-cancel" type="button" class="dark-cancel-button">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
