<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Список дней рождения</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/list-table.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/birthdays-list.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script type="module" src="{{asset('assets/compiled-js/birthdays-list.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="flex-between">
            <div @class(['hidden' => empty($result->birthdays)])>
                <span class="input-label">
                    Сортировать по
                </span>
                <select id="sorting-select" class="dark-select">
                    <option value="{{\App\Models\Enums\BirthdaySorting::PERSON_NAME->value}}">
                        имени
                    </option>
                    <option value="{{\App\Models\Enums\BirthdaySorting::DAY_AND_MONTH->value}}">
                        дню и месяцу рождения
                    </option>
                </select>
            </div>
            <form method="get" action="{{route('birthdays.create')}}">
                <button type="submit" class="dark-create-button button-icon-flex">
                    <img src="{{asset('assets/img/icons/plus.png')}}">
                    <span>
                        Новый день рождения
                    </span>
                </button>
            </form>
        </div>
        @if(count($result->birthdays) == 0)
            <div class="empty-message-flex">
                <div class="empty-message">
                    Дни рождения отсутствуют
                </div>
            </div>
        @else
            <div class="flex-center">
                <table class="list-table">
                    <thead>
                    <tr>
                        <td>
                            Имя
                        </td>
                        <td>
                            Фото
                        </td>
                        <td>
                            Дата рождения
                        </td>
                        <td>
                            Возраст
                        </td>
                        <td></td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($result->birthdays as $birthday)
                        <tr>
                            <td>
                                {{$birthday->personName}}
                            </td>
                            <td class="centered-cell">
                                @isset($birthday->photoUrl)
                                    <img class="person-photo" src="{{$birthday->photoUrl}}">
                                @endisset
                            </td>
                            <td class="centered-cell">
                                {{$birthday->date}}
                            </td>
                            <td class="centered-cell">
                                @isset($birthday->age)
                                    {{$birthday->age}}
                                @endisset
                            </td>
                            <td class="buttons-cell">
                                <form method="get" action="{{route('birthdays.edit', ['birthday' => $birthday->id])}}"
                                      class="inline">
                                    <button type="submit" class="dark-button tight-button">
                                        <img src="{{asset('assets/img/icons/edit.png')}}">
                                    </button>
                                </form>
                                <form method="post"
                                      action="{{route('birthdays.destroy', ['birthday' => $birthday->id])}}"
                                      class="inline">
                                    @method('DELETE')
                                    @csrf
                                    <button id="btn-delete-birthday-{{$birthday->id}}"
                                            type="button" class="dark-button tight-button ml">
                                        <img src="{{asset('assets/img/icons/delete.png')}}">
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        @endif
    </div>
</div>
</body>
</html>
