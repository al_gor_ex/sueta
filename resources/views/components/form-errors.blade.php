@if($errors->any())
    <div class="flex-center">
        <div class="error-messages-container">
            @foreach($errors->all() as $error)
                <div>
                    {{$error}}
                </div>
            @endforeach
        </div>
    </div>
@endif
