<script type="module" src="{{asset('assets/compiled-js/navbar.js')}}"></script>
<div class="navbar">
    <div class="logo">
        SUETA
    </div>
    <div class="separator"></div>
    @auth
        <div class="user-name">
            {{\Illuminate\Support\Facades\Auth::user()->name}}
        </div>
        <div class="separator"></div>
        <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'notifications.index'])
           href="{{route('notifications.index')}}">
            <div class="link-flex">
                <img src="{{asset('assets/img/icons/notification.png')}}">
                <span class="link-text">
                Уведомления
            </span>
            </div>
        </a>
        <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'calendar'])
           href="{{route('calendar')}}">
            <div class="link-flex">
                <img src="{{asset('assets/img/icons/calendar.png')}}">
                <span class="link-text">
                Календарь
            </span>
            </div>
        </a>
        <div class="separator"></div>
        <div class="submenu-header">
            Реестр
        </div>
        <div class="submenu-wrapper">
            <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'events.index'])
               href="{{route('events.index')}}">
                <div class="link-flex">
                    <img src="{{asset('assets/img/icons/event.png')}}">
                    <span class="link-text">
                    Мероприятия
                </span>
                </div>
            </a>
            <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'routines.index'])
               href="{{route('routines.index')}}">
                <div class="link-flex">
                    <img src="{{asset('assets/img/icons/routine.png')}}">
                    <span class="link-text">
                    Рутина
                </span>
                </div>
            </a>
            <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'tasks.index'])
               href="{{route('tasks.index')}}">
                <div class="link-flex">
                    <img src="{{asset('assets/img/icons/task.png')}}">
                    <span class="link-text">
                    Задачи
                </span>
                </div>
            </a>
            <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'birthdays.index'])
               href="{{route('birthdays.index')}}">
                <div class="link-flex">
                    <img src="{{asset('assets/img/icons/birthday.png')}}">
                    <span class="link-text">
                    Дни рождения
                </span>
                </div>
            </a>
        </div>
        <div class="separator"></div>
        <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'help'])
           href="{{route('help')}}">
            <div class="link-flex">
                <img src="{{asset('assets/img/icons/question.png')}}">
                <span class="link-text">
                Справка
            </span>
            </div>
        </a>
        <div class="separator"></div>
        <form class="link" method="get" action="{{route('logout')}}">
            <div id="btn-logout" class="link-flex">
                <img src="{{asset('assets/img/icons/logout.png')}}">
                <span class="link-text">
                    Выход
                </span>
            </div>
        </form>
    @endauth
    @guest
        <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'login.form'])
            href="{{route('login.form')}}">
            <div class="link-flex">
                <img src="{{asset('assets/img/icons/login.png')}}">
                <span class="link-text">
                Вход
            </span>
            </div>
        </a>
        <a @class(['link', 'current-link' => \Illuminate\Support\Facades\Route::currentRouteName() == 'registration.form'])
            href="{{route('registration.form')}}">
            <div class="link-flex">
                <img src="{{asset('assets/img/icons/registration.png')}}">
                <span class="link-text">
                Регистрация
            </span>
            </div>
        </a>
    @endguest
</div>
