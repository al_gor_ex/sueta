<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Вход</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/login.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post" action="{{route('login')}}" autocomplete="off" class="login-form">
            @csrf
            @if(session('failed'))
                <div class="flex-center">
                    <div class="error-messages-container">
                        Неверные данные
                    </div>
                </div>
            @endif
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Email
                    </div>
                    <div>
                        <input name="email" type="text" value="{{old('email')}}" class="dark-text-input" required>
                    </div>
                    <div class="input-label">
                        Пароль
                    </div>
                    <div>
                        <input name="password" type="password" value="{{old('password')}}" class="dark-text-input"
                               required>
                    </div>
                    <div class="checkbox-flex">
                        <input id="remember_me" name="remember" type="checkbox" class="dark-checkbox checkbox-margin">
                        <label for="remember_me" class="input-label checkbox-label">
                            Запомнить меня
                        </label>
                    </div>
                    <div class="button-flex">
                        <button type="submit" class="dark-button">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
