<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Регистрация</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/registration.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <form method="post" action="{{route('register')}}" autocomplete="off" class="registration-form">
            @include('components.form-errors')
            @csrf
            <div class="flex-center">
                <div>
                    <div class="input-label">
                        Имя
                    </div>
                    <div>
                        <input name="name" type="text" value="{{old('name')}}" required autofocus
                               class="dark-text-input">
                    </div>
                    <div class="input-label">
                        Email
                    </div>
                    <div>
                        <input name="email" type="text" value="{{old('email')}}" class="dark-text-input" required>
                    </div>
                    <div class="input-label">
                        Пароль
                    </div>
                    <div>
                        <input name="password" type="password" class="dark-text-input" required>
                    </div>
                    <div class="input-label">
                        Повторите пароль
                    </div>
                    <div>
                        <input name="password_confirmation" type="password" class="dark-text-input" required>
                    </div>
                    <div class="button-flex">
                        <button type="submit" class="dark-button">
                            Отправить
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
