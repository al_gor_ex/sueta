<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Календарь</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/calendar.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/calendar.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="title-flex">
            <div id="btn-prev-month" class="dark-button tight-button">
                <img src="{{asset('assets/img/icons/arrow-left.png')}}">
            </div>
            <div class="month-label">
                {{$calendar->periodName}}
            </div>
            <div id="btn-next-month" class="dark-button tight-button">
                <img src="{{asset('assets/img/icons/arrow-right.png')}}">
            </div>
        </div>
        <table class="calendar">
            <thead>
            <tr class="calendar-header">
                <td>ПН</td>
                <td>ВТ</td>
                <td>СР</td>
                <td>ЧТ</td>
                <td>ПТ</td>
                <td>СБ</td>
                <td>ВС</td>
            </tr>
            </thead>
            <tbody>
            @foreach($calendar->cells as $row)
                <tr>
                    @foreach($row as $cell)
                    <td @class(['current-day' => $cell->isCurrentDate, 'busy-day' => !($cell->isCurrentDate) && count($cell->events) > 0])>
                        <div class="cell-flex">
                            <div class="day-number">
                                @isset($cell->dayNumber)
                                    {{$cell->dayNumber}}
                                @endisset
                            </div>
                            <div>
                                @foreach($cell->events as $event)
                                    <div class="task">
                                        @switch($event->type)
                                            @case(\App\Models\Enums\EventType::BIRTHDAY)
                                                <img src="{{asset('assets/img/icons/birthday.png')}}">
                                                @break
                                            @case(\App\Models\Enums\EventType::EVENT)
                                                <img src="{{asset('assets/img/icons/event.png')}}">
                                                @break
                                            @case(\App\Models\Enums\EventType::ROUTINE)
                                                <img src="{{asset('assets/img/icons/routine.png')}}">
                                                @break
                                        @endswitch
                                        <span class="task-description">
                                            {{$event->description}}
                                        </span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>
