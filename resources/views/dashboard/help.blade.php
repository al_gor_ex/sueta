<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Справка</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/help.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        <div class="help-article">
            <div class="help-header">
                Термины, используемые в приложении
            </div>
            <div class="paragraph">
                <div class="term-flex">
                    <img src="{{asset('assets/img/icons/event.png')}}">
                    <span class="term-description">
                        <span class="term">
                            Мероприятие
                        </span>
                        &mdash; событие, которое должно произойти в определённую дату и время
                    </span>
                </div>
                <div class="example">
                    Пример: встреча выпускников 10 июля 2017 в 17:00
                </div>
            </div>
            <div class="paragraph">
                <div class="term-flex">
                    <img src="{{asset('assets/img/icons/routine.png')}}">
                    <span class="term-description">
                        <span class="term">
                            Рутина
                        </span>
                        &mdash; задача, которую нужно выполнять с интервалом в некоторое кол-во дней
                    </span>
                </div>
                <div class="example">
                    Пример: 1 раз в месяц оплатить счета
                </div>
            </div>
            <div class="paragraph">
                <div class="term-flex">
                    <img src="{{asset('assets/img/icons/task.png')}}">
                    <span class="term-description">
                        <span class="term">
                            Задача
                        </span>
                        &mdash; цель, имеющая определённый приоритет и относящаяся к некоторой смысловой категории
                    </span>
                </div>
                <div class="example">
                    Пример: отремонтировать смеситель в ванной
                </div>
            </div>
            <div class="paragraph">
                <div class="term-flex">
                    <img src="{{asset('assets/img/icons/birthday.png')}}">
                    <span class="term-description">
                        <span class="term">
                            День рождения
                        </span>
                        &mdash; необходимость поздравить определённого человека в указанный день
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
