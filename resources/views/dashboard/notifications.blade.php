<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>SUETA - Уведомления</title>
    <link rel="stylesheet" href="{{asset('assets/css/navbar.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/controls.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/notifications.css')}}">
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}">
    <script defer src="{{asset('assets/compiled-js/notifications.js')}}"></script>
</head>
<body>
<div class="wrapper">
    @include('components.navbar')
    <div class="main-area">
        @if(empty($result->notificationBatches))
            <div class="empty-message-flex">
                <div class="empty-message">
                    Уведомления отсутствуют
                </div>
            </div>
        @else
            <div class="status-flex">
                <div class="counters">
                    <span class="pending-label">
                        Ожидают выполнения:
                    </span>
                    <span class="counter pending-counter">
                        {{$result->pendingCount}}
                    </span>
                    <span class="out-of-date-label">
                        Из них просроченных:
                    </span>
                    <span class="counter out-of-date-counter">
                        {{$result->outdatedCount}}
                    </span>
                </div>
                <form class="inline" method="post" action="{{route('notifications.clear')}}">
                    @method('DELETE')
                    @csrf
                    <button id="btn-clear-notifications" type="button" class="dark-cancel-button button-icon-flex">
                        <img src="{{asset('assets/img/icons/trash.png')}}">
                        <span>
                            Очистить
                        </span>
                    </button>
                </form>
            </div>
            <div class="notifications-area">
                @foreach($result->notificationBatches as $dayBatch)
                    <div id="day-header-{{$loop->index}}"
                        @class(['day-header', 'pending-border' => $dayBatch->hasPendingNotifications,
                                    'out-of-date-border' => $dayBatch->hasOutdatedNotifications])>
                        <span>
                            {{$dayBatch->title}}
                        </span>
                        @php
                        $isDisplayed = $dayBatch->hasPendingNotifications || $dayBatch->hasOutdatedNotifications;
                        @endphp
                        <span class="day-collapse-mark">
                            {{$isDisplayed ? '-' : '+'}}
                        </span>
                    </div>
                    <div id="day-container-{{$loop->index}}" @class(['day-container', 'display-none' => !$isDisplayed])>
                        @foreach($dayBatch->notifications as $notification)
                            <div @class(['notification',
                                        'pending-border' => !($notification->isConfirmed) && $dayBatch->hasPendingNotifications,
                                        'out-of-date-border' => !($notification->isConfirmed) && $dayBatch->hasOutdatedNotifications])>
                                <div class="notification-column">
                                    @switch($notification->eventType)
                                        @case(\App\Models\Enums\EventType::BIRTHDAY)
                                            <img src="{{asset('assets/img/big-icons/birthday.png')}}" class="notification-icon">
                                            @break
                                        @case(\App\Models\Enums\EventType::EVENT)
                                            <img src="{{asset('assets/img/big-icons/event.png')}}" class="notification-icon">
                                            @break
                                        @case(\App\Models\Enums\EventType::ROUTINE)
                                            <img src="{{asset('assets/img/big-icons/routine.png')}}" class="notification-icon">
                                            @break
                                    @endswitch
                                </div>
                                <div @class(['notification-column', 'done' => $notification->isConfirmed])>
                                    @switch($notification->eventType)
                                        @case(\App\Models\Enums\EventType::BIRTHDAY)
                                            День рождения
                                            @break
                                        @case(\App\Models\Enums\EventType::EVENT)
                                            Мероприятие
                                            @break
                                        @case(\App\Models\Enums\EventType::ROUTINE)
                                            Рутина
                                            @break
                                    @endswitch
                                </div>
                                <div @class(['notification-column', 'done' => $notification->isConfirmed])>
                                    {{$notification->description}}
                                </div>
                                <div class="notification-column">
                                    @isset($notification->photoUrl)
                                        <img src="{{$notification->photoUrl}}" class="person-photo">
                                    @endisset
                                </div>
                                <div class="notification-column">
                                    @if($notification->isConfirmed == false)
                                        <form method="post"
                                              action="{{route('notifications.confirm', ['id' => $notification->id])}}">
                                            @method('PUT')
                                            @csrf
                                            <button id="confirm-notification-{{$notification->id}}"
                                                    type="button" class="dark-button">
                                                Подтвердить
                                            </button>
                                        </form>
                                    @endif
                                </div>
                            </div>
                            <div class="notification-separator"></div>
                        @endforeach
                    </div>
                @endforeach
            </div>

            <div class="pagination-flex">
                <div id="btn-prev-page" @class(['dark-button', 'tight-button', 'hidden' => $result->currentPage == 1])>
                    <img src="{{asset('assets/img/icons/arrow-left.png')}}">
                </div>
                <div class="pagination-label">
                    {{$result->currentPage}} / {{$result->totalPages}}
                </div>
                <div id="btn-next-page"
                    @class(['dark-button', 'tight-button', 'hidden' => $result->currentPage == $result->totalPages])>
                    <img src="{{asset('assets/img/icons/arrow-right.png')}}">
                </div>
            </div>
        @endif
    </div>
</div>
</body>
</html>
