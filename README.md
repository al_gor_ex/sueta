# Sueta - планировщик задач

Приложение позволяет вести учёт:

- Планируемых мероприятий
- Рутинных обязанностей
- Дней рождения знакомых
- Задач, сгруппированных по категориям

Пользователь планирует свой график, исходя из:

- Уведомлений о задачах на текущий день и просроченных уведомлений
- Календаря, позволяющего просмотреть предстоящие задачи на месяц и более

## ER-диаграмма

![](https://sun9-2.userapi.com/impg/jz4NDYeVCi9Q5AYkxd488V-0SnmvJ3DAwliA-A/PO9cjEtSUl8.jpg?size=1027x462&quality=95&sign=bbcd8b1e21e165077ef6290554c1231d&type=album)

## Скриншоты

![](https://sun9-67.userapi.com/impg/scccI-0pscf63fXWmPagOlv4vaN7CMYeabwkvg/rpdYp4FqK5Y.jpg?size=1300x624&quality=95&sign=fdeddc1326b86d84d3c55a91b992ccdb&type=album)

![](https://sun9-25.userapi.com/impg/w-4jQ-_t-Ft2sIeviajdXLYwkTkMPcCCJF1VPg/dX_XzzbBtyQ.jpg?size=1301x626&quality=95&sign=123487cf08d1822aae1cccaddeaedb93&type=album)

![](https://sun9-78.userapi.com/impg/AzRtMUauAovFy8VLdQ3EswSKpDAIc-_orOn0Zw/urev31oSwZc.jpg?size=1299x613&quality=95&sign=2a61a0fb47f673d84ff7746a6415976b&type=album)

![](https://sun9-73.userapi.com/impg/BgJSwjJyvZHryUdctWWqHh7GzSwLlVo3FMFLDg/P11IoFvEpcQ.jpg?size=1299x624&quality=95&sign=27929dd1d50de1d622739382352c18a1&type=album)

![](https://sun9-80.userapi.com/impg/LZD6tpMeQ5SE3Hi0zWy2mjNbvOqIRHZ1rgXhSA/XCxatARMaAU.jpg?size=1302x624&quality=95&sign=9fc243fab124e18cc0c2d4c6e145d9b8&type=album)

![](https://sun9-6.userapi.com/impg/PcIeA-DUSNUZdLs2A6fSQA8KPdbGp0_aYS7FmA/UzzYVVsRmS0.jpg?size=1301x621&quality=95&sign=28c64b09b769f53ef56d11910f9a9df7&type=album)

![](https://sun9-88.userapi.com/impg/r-0PRg_UDs9DRVRjOzvpmpVvBKz1EPz21QJYFw/cVvP5HI1OIs.jpg?size=1301x619&quality=95&sign=8a88bab2cdcdf053eb19468e2bb1c478&type=album)

![](https://sun9-84.userapi.com/impg/sCfyfmmQrp-U7TK_SpHujJLa8vvEHZE279d2kg/mBuU_aTGTUc.jpg?size=1302x623&quality=95&sign=382a41a04155f754cc6149dc6c6ba361&type=album)

![](https://sun9-14.userapi.com/impg/1b0w4WNprH7eIe99bfvkvEiM_Dpq3nGR24QEhg/9trGBPX8Ndw.jpg?size=1298x625&quality=95&sign=a09f103289d5c4320826bea257b82c94&type=album)

![](https://sun9-19.userapi.com/impg/SDNjfwXFZ8uDcI7x2IbLSyTaWkociQDsRvYXSw/zvNECGod1Qw.jpg?size=1301x624&quality=95&sign=abbf24b296ac01ed0ce3d0ade090ab00&type=album)

![](https://sun9-26.userapi.com/impg/iGvzwuAXac_vAtllo5fLmnOPx5E0IZ7phHJplA/pV7PqhRudsc.jpg?size=1301x624&quality=95&sign=28ddd312e3dc6ae8eb3fd157f24a3064&type=album)

![](https://sun9-34.userapi.com/impg/5RfNIA7lNj-nu13WuShqIl_ZMKUK-9RoVwnP8w/BF_tnPvkR74.jpg?size=1302x625&quality=95&sign=52644e408ea0a1dbc4ba115f4e31161e&type=album)

![](https://sun9-76.userapi.com/impg/_dxUBMy0UcdtyKR1qVUchiFw38shhpEv0yvZGQ/sMS_j5FCdGU.jpg?size=1299x626&quality=95&sign=dd94503a262b44acdb1ff66930c982db&type=album)

![](https://sun9-8.userapi.com/impg/rzkhWoL5gn88Gk98Taw7KYI1wf10h00caT4pmg/k-SUGhYIetU.jpg?size=1298x624&quality=95&sign=3d7d9324501868128cb99925ee279eb9&type=album)