<?php

use App\Http\Controllers\BirthdayController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\RoutineController;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login.form');
});
Route::middleware('guest')->controller(UserController::class)->group(function () {
    Route::prefix('/register')->group(function () {
        Route::get('/', 'create')->name('registration.form');
        Route::post('/', 'store')->name('register');
    });
    Route::prefix('/login')->group(function () {
        Route::view('/', 'auth.login')->name('login.form');
        Route::post('/', 'login')->name('login');
    });
});

Route::middleware('auth')->group(function () {
    Route::get('/logout', [UserController::class, 'logout'])->name('logout');
    Route::delete('/events/delete-past', [EventController::class, 'deletePast'])->name('events.delete-past');
    Route::resource('events', EventController::class)->except(['show']);
    Route::resource('routines', RoutineController::class)->except(['show']);
    Route::resource('birthdays', BirthdayController::class)->except(['show']);
    Route::resource('categories', CategoryController::class)->except(['show']);
    Route::put('/tasks/{id}/toggle-finished', [TaskController::class, 'toggleFinished']);
    Route::delete('/tasks/delete-finished', [TaskController::class, 'deleteFinished'])->name('tasks.delete-finished');
    Route::resource('tasks', TaskController::class)->except(['show']);
    Route::view('/help', 'dashboard.help')->name('help');
    Route::get('/calendar', [CalendarController::class, 'index'])->name('calendar');
    Route::prefix('/notifications')
        ->name('notifications.')
        ->controller(NotificationController::class)
        ->group(function() {
            Route::get('/', 'index')->name('index');
            Route::put('/{id}/confirm', 'confirm')->name('confirm');
            Route::delete('/clear', 'clear')->name('clear');
        });
});
